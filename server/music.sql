-- MySQL dump 10.13  Distrib 5.7.21, for Linux (x86_64)
--
-- Host: localhost    Database: music
-- ------------------------------------------------------
-- Server version	5.7.21-0ubuntu0.16.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `aggregated_artists`
--

DROP TABLE IF EXISTS `aggregated_artists`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `aggregated_artists` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `artist_id` int(10) unsigned NOT NULL,
  `aggregator_id` int(10) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `aggregated_artists`
--

LOCK TABLES `aggregated_artists` WRITE;
/*!40000 ALTER TABLE `aggregated_artists` DISABLE KEYS */;
/*!40000 ALTER TABLE `aggregated_artists` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `aggregators`
--

DROP TABLE IF EXISTS `aggregators`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `aggregators` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_info_id` int(10) unsigned NOT NULL,
  `bio` text COLLATE utf8mb4_unicode_ci,
  `address` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `cac_number` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `aggregators_name_unique` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `aggregators`
--

LOCK TABLES `aggregators` WRITE;
/*!40000 ALTER TABLE `aggregators` DISABLE KEYS */;
/*!40000 ALTER TABLE `aggregators` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `albums`
--

DROP TABLE IF EXISTS `albums`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `albums` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_info_id` int(10) unsigned NOT NULL,
  `content_code` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `release_date` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `artist_id` int(10) unsigned NOT NULL DEFAULT '0',
  `fully_scraped` tinyint(1) NOT NULL DEFAULT '0',
  `temp_id` varchar(8) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `views` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `albums_name_artist_id_unique` (`name`,`artist_id`),
  UNIQUE KEY `albums_content_code_unique` (`content_code`),
  KEY `albums_release_date_index` (`release_date`),
  KEY `albums_artist_id_index` (`artist_id`),
  KEY `albums_temp_id_index` (`temp_id`),
  KEY `albums_fully_scraped_index` (`fully_scraped`),
  KEY `albums_views_index` (`views`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `albums`
--

LOCK TABLES `albums` WRITE;
/*!40000 ALTER TABLE `albums` DISABLE KEYS */;
INSERT INTO `albums` VALUES (1,'Truth is a Beautiful thing',0,'','2017-03-21','http://78.157.209.138:9000/images/5SyVSLNRY5HtLzVdtehEpfKbEK6H1paBpxjlObgi.png',2,0,NULL,1);
/*!40000 ALTER TABLE `albums` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `artists`
--

DROP TABLE IF EXISTS `artists`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `artists` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_info_id` int(10) unsigned NOT NULL,
  `image_small` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image_large` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `fully_scraped` tinyint(1) NOT NULL DEFAULT '0',
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `bio` text COLLATE utf8mb4_unicode_ci,
  `wiki_image_large` mediumtext COLLATE utf8mb4_unicode_ci,
  `wiki_image_small` mediumtext COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `views` int(11) NOT NULL DEFAULT '0',
  `username` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `artists_name_unique` (`name`),
  KEY `artists_fully_scraped_index` (`fully_scraped`),
  KEY `artists_views_index` (`views`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `artists`
--

LOCK TABLES `artists` WRITE;
/*!40000 ALTER TABLE `artists` DISABLE KEYS */;
INSERT INTO `artists` VALUES (2,'London Grammar',18,'http://localhost:8010/storage/artist_images/9C4bVEDTS4qf4uRZwzZJk3owuKH2S6Tepr4VYtQ8.jpeg','http://localhost:8010/storage/artist_images/O1wcFxDj5lcEonegPpbIPnFVJYc8H1oJOKapwxGi.jpeg',1,'2018-03-21 10:56:05','{\"bio\":\"\",\"images\":[{\"url\":\"\"}]}',NULL,NULL,'2018-03-21 10:56:05',0,'stikks');
/*!40000 ALTER TABLE `artists` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `follows`
--

DROP TABLE IF EXISTS `follows`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `follows` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `follower_id` int(11) NOT NULL,
  `followed_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `follows_follower_id_followed_id_unique` (`follower_id`,`followed_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `follows`
--

LOCK TABLES `follows` WRITE;
/*!40000 ALTER TABLE `follows` DISABLE KEYS */;
/*!40000 ALTER TABLE `follows` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `genre_artist`
--

DROP TABLE IF EXISTS `genre_artist`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `genre_artist` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `genre_id` int(11) NOT NULL,
  `artist_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `genre_artist_artist_id_genre_id_unique` (`artist_id`,`genre_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `genre_artist`
--

LOCK TABLES `genre_artist` WRITE;
/*!40000 ALTER TABLE `genre_artist` DISABLE KEYS */;
INSERT INTO `genre_artist` VALUES (1,1,1),(2,1,2),(3,2,2),(4,3,2);
/*!40000 ALTER TABLE `genre_artist` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `genres`
--

DROP TABLE IF EXISTS `genres`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `genres` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `genres_name_unique` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `genres`
--

LOCK TABLES `genres` WRITE;
/*!40000 ALTER TABLE `genres` DISABLE KEYS */;
INSERT INTO `genres` VALUES (1,'Alternative','2018-03-21 10:45:59','2018-03-21 10:45:59'),(2,'Pop','2018-03-22 13:21:32','2018-03-22 13:21:32'),(3,'Rock','2018-03-22 13:21:32','2018-03-22 13:21:32'),(4,'Makosa',NULL,NULL);
/*!40000 ALTER TABLE `genres` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `localizations`
--

DROP TABLE IF EXISTS `localizations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `localizations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `lines` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `localizations_name_index` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `localizations`
--

LOCK TABLES `localizations` WRITE;
/*!40000 ALTER TABLE `localizations` DISABLE KEYS */;
INSERT INTO `localizations` VALUES (1,'English','{\"Toggle Sidebar\":\"Toggle Sidebar\",\"Admin Area\":\"Admin Area\",\"Account Settings\":\"Account Settings\",\"Logout\":\"Logout\",\"Analytics\":\"Analytics\",\"Appearance\":\"Appearance\",\"Settings\":\"Settings\",\"Artists\":\"Artists\",\"Albums\":\"Albums\",\"Tracks\":\"Tracks\",\"Lyrics\":\"Lyrics\",\"Playlists\":\"Playlists\",\"Users\":\"Users\",\"Groups\":\"Groups\",\"Pages\":\"Pages\",\"Translations\":\"Translations\",\"Mail Templates\":\"Mail Templates\",\"Ads\":\"Ads\",\"Predefined AD Slots\":\"Predefined AD Slots\",\"Slot #1\":\"Slot #1\",\"This ad will appear at the top of most pages. Best size <= 150px height or responsive.\":\"This ad will appear at the top of most pages. Best size <= 150px height or responsive.\",\"Slot #2\":\"Slot #2\",\"This ad will appear at the bottom of most pages. Best size <= 150px height or responsive.\":\"This ad will appear at the bottom of most pages. Best size <= 150px height or responsive.\",\"Slot #3\":\"Slot #3\",\"This ad will appear in artist page only (below popular songs). Best size <= 1000px width or responsive.\":\"This ad will appear in artist page only (below popular songs). Best size <= 1000px width or responsive.\",\"Slot #4\":\"Slot #4\",\"This ad will appear in artist page only (below similar artists). Best size <= 430px width or responsive.\":\"This ad will appear in artist page only (below similar artists). Best size <= 430px width or responsive.\",\"Slot #5\":\"Slot #5\",\"This ad will appear in album page only (above album tracks). Best size is as wide as possible or responsive.\":\"This ad will appear in album page only (above album tracks). Best size is as wide as possible or responsive.\",\"Disable Ads\":\"Disable Ads\",\"Disable all ad related functionality.\":\"Disable all ad related functionality.\",\"Save\":\"Save\",\"Search albums\":\"Search albums\",\"Albums Selected\":\"Albums Selected\",\"Delete\":\"Delete\",\"Name\":\"Name\",\"Release Date\":\"Release Date\",\"Popularity\":\"Popularity\",\"Number of Tracks\":\"Number of Tracks\",\"Edit Album\":\"Edit Album\",\"Nothing To Display.\":\"Nothing To Display.\",\"Seems like no albums have been created yet.\":\"Seems like no albums have been created yet.\",\"New Track\":\"New Track\",\"Search tracks\":\"Search tracks\",\"Tracks Selected\":\"Tracks Selected\",\"Number\":\"Number\",\"Duration\":\"Duration\",\"Edit Track\":\"Edit Track\",\"No tracks have been attached to this album yet.\":\"No tracks have been attached to this album yet.\",\"Update\":\"Update\",\"Create\":\"Create\",\"Image\":\"Image\",\"Upload image\":\"Upload image\",\"Total Users\":\"Total Users\",\"Total Songs\":\"Total Songs\",\"Total Artists\":\"Total Artists\",\"Total Albums\":\"Total Albums\",\"Users Online\":\"Users Online\",\"Customizing\":\"Customizing\",\"Appearance Editor\":\"Appearance Editor\",\"Saved\":\"Saved\",\"Code Editor\":\"Code Editor\",\"Cancel\":\"Cancel\",\"Modify\":\"Modify\",\"Change\":\"Change\",\"Add\":\"Add\",\"Remove\":\"Remove\",\"Use Default\":\"Use Default\",\"New Menu Item\":\"New Menu Item\",\"Link\":\"Link\",\"URL\":\"URL\",\"Link Text\":\"Link Text\",\"Route\":\"Route\",\"Page\":\"Page\",\"Menu Items\":\"Menu Items\",\"This menu has no items yet.\":\"This menu has no items yet.\",\"Label\":\"Label\",\"Type\":\"Type\",\"Action\":\"Action\",\"Condition\":\"Condition\",\"None\":\"None\",\"Show to Logged in Users Only\":\"Show to Logged in Users Only\",\"Show to Guest Users Only\":\"Show to Guest Users Only\",\"Show to Admin Only\":\"Show to Admin Only\",\"Show to Agents Only\":\"Show to Agents Only\",\"Icon\":\"Icon\",\"Add a Menu\":\"Add a Menu\",\"Menu Name\":\"Menu Name\",\"Menu Position\":\"Menu Position\",\"Primary\":\"Primary\",\"Secondary\":\"Secondary\",\"Mobile Bottom\":\"Mobile Bottom\",\"Delete Menu\":\"Delete Menu\",\"Add Items\":\"Add Items\",\"Search artists\":\"Search artists\",\"Artists Selected\":\"Artists Selected\",\"Number of Albums\":\"Number of Albums\",\"Edit Artist\":\"Edit Artist\",\"Seems like no artists have been created yet.\":\"Seems like no artists have been created yet.\",\"New Album\":\"New Album\",\"\":\"\",\"No albums have been attached to this artist yet.\":\"No albums have been attached to this artist yet.\",\"Small Image\":\"Small Image\",\"Large Image\":\"Large Image\",\"Genres\":\"Genres\",\"Add new...\":\"Add new...\",\"Biography\":\"Biography\",\"Biography Images\":\"Biography Images\",\"Separate images with a new line (enter).\":\"Separate images with a new line (enter).\",\"Assign users to:\":\"Assign users to:\",\"Could not find any users with these email addresses.\":\"Could not find any users with these email addresses.\",\"Assign More\":\"Assign More\",\"Assign\":\"Assign\",\"Update Group\":\"Update Group\",\"Create a New Group\":\"Create a New Group\",\"Group Permissions\":\"Group Permissions\",\"No permissions yet.\":\"No permissions yet.\",\"Default\":\"Default\",\"Assign this group to new users automatically.\":\"Assign this group to new users automatically.\",\"Guests\":\"Guests\",\"Assign this group to guests (not logged in users).\":\"Assign this group to guests (not logged in users).\",\"New Group\":\"New Group\",\"Edit\":\"Edit\",\"Assign Users\":\"Assign Users\",\"Unassign Users\":\"Unassign Users\",\"Search users\":\"Search users\",\"Avatar\":\"Avatar\",\"Email\":\"Email\",\"First Name\":\"First Name\",\"Last Name\":\"Last Name\",\"There are no users in this group yet.\":\"There are no users in this group yet.\",\"Guests Group\":\"Guests Group\",\"Users can\'t be assigned to this group.\":\"Users can\'t be assigned to this group.\",\"Update Lyric\\n            New Lyric\":\"Update Lyric\\n            New Lyric\",\"Update Lyric\":\"Update Lyric\",\"New Lyric\":\"New Lyric\",\"Close\":\"Close\",\"Update\\n            Create\":\"Update\\n            Create\",\"Search Lyrics\":\"Search Lyrics\",\"Lyric Selected\":\"Lyric Selected\",\"Song\":\"Song\",\"Artist\":\"Artist\",\"Album\":\"Album\",\"Last Updated\":\"Last Updated\",\"Edit Lyric\":\"Edit Lyric\",\"Seems like no lyrics have been created yet.\":\"Seems like no lyrics have been created yet.\",\"Inbox\":\"Inbox\",\"to me\":\"to me\",\"Selected Template\":\"Selected Template\",\"Subject\":\"Subject\",\"Plaintext\":\"Plaintext\",\"Restore Default\":\"Restore Default\",\"Mail template syntax is not valid.\":\"Mail template syntax is not valid.\",\"Back to pages\":\"Back to pages\",\"Search Pages\":\"Search Pages\",\"Pages Selected\":\"Pages Selected\",\"Slug\":\"Slug\",\"Body\":\"Body\",\"Created At\":\"Created At\",\"Edit Page\":\"Edit Page\",\"Seems like no pages have been created yet.\":\"Seems like no pages have been created yet.\",\"to\":\"to\",\"of\":\"of\",\"Per Page:\":\"Per Page:\",\"New Playlist\":\"New Playlist\",\"Search Playlists\":\"Search Playlists\",\"Playlist Selected\":\"Playlist Selected\",\"Creator\":\"Creator\",\"Public\":\"Public\",\"Views\":\"Views\",\"Edit Playlist\":\"Edit Playlist\",\"Seems like no playlists have been created yet.\":\"Seems like no playlists have been created yet.\",\"Configure google analytics integration and credentials.\":\"Configure google analytics integration and credentials.\",\"Google Analytics Application ID\":\"Google Analytics Application ID\",\"Google application ID. Can be the same one used for google social login.\":\"Google application ID. Can be the same one used for google social login.\",\"Google Analytics Tracking Code\":\"Google Analytics Tracking Code\",\"Google analytics tracking code only, not the whole javascript code snippet.\":\"Google analytics tracking code only, not the whole javascript code snippet.\",\"Authentication\":\"Authentication\",\"Configure registration, social login  and related 3rd party integrations.\":\"Configure registration, social login  and related 3rd party integrations.\",\"Require Email Confirmation\":\"Require Email Confirmation\",\"Require new users to validate their email address before being able to login.\":\"Require new users to validate their email address before being able to login.\",\"Disable Registration\":\"Disable Registration\",\"All registration (including social login) will be disabled.\":\"All registration (including social login) will be disabled.\",\"Google Login\":\"Google Login\",\"Enable logging into the site via google.\":\"Enable logging into the site via google.\",\"Google ID\":\"Google ID\",\"Google Secret\":\"Google Secret\",\"Facebook Login\":\"Facebook Login\",\"Enable logging into the site via facebook.\":\"Enable logging into the site via facebook.\",\"Facebook ID\":\"Facebook ID\",\"Facebook Secret\":\"Facebook Secret\",\"Twitter Login\":\"Twitter Login\",\"Twitter ID\":\"Twitter ID\",\"Twitter Secret\":\"Twitter Secret\",\"Blocked Artist\":\"Blocked Artist\",\"Artists that should be blocked on the site (they will not be searchable, indexable, or appear on the site in any other way.\":\"Artists that should be blocked on the site (they will not be searchable, indexable, or appear on the site in any other way.\",\"+Artist Name\":\"+Artist Name\",\"Cache\":\"Cache\",\"Configure cache time, method and related 3rd party integrations.\":\"Configure cache time, method and related 3rd party integrations.\",\"Cache Method\":\"Cache Method\",\"Which method should be used for storing and retrieving cached items.\":\"Which method should be used for storing and retrieving cached items.\",\"Every Day\":\"Every Day\",\"Every 3 Days\":\"Every 3 Days\",\"Every Week\":\"Every Week\",\"Every 2 Weeks\":\"Every 2 Weeks\",\"Every Month\":\"Every Month\",\"Never\":\"Never\",\"Memcached Host\":\"Memcached Host\",\"Memcached Port\":\"Memcached Port\",\"Clear Cache\":\"Clear Cache\",\"Important!\":\"Important!\",\"\\\"File\\\" is the best option for most cases and should not be changed, unless you are familiar with another cache method and have it set up on the server already.\":\"\\\"File\\\" is the best option for most cases and should not be changed, unless you are familiar with another cache method and have it set up on the server already.\",\"Manage envato integration module.\":\"Manage envato integration module.\",\"Envato Integration\":\"Envato Integration\",\"Enable envato integration (social login, purchase code validation, sales reports and more)\":\"Enable envato integration (social login, purchase code validation, sales reports and more)\",\"Envato Purchase Code\":\"Envato Purchase Code\",\"Require users to enter a valid purchase code in order to register and submit support requests.\\n            Envato social login will also check for purchase code automatically when this is enabled.\":\"Require users to enter a valid purchase code in order to register and submit support requests.\\n            Envato social login will also check for purchase code automatically when this is enabled.\",\"Filter Help Center Search\":\"Filter Help Center Search\",\"Users will only be able to find articles for items that they have purchased.\":\"Users will only be able to find articles for items that they have purchased.\",\"Envato Items\":\"Envato Items\",\"This will automatically import all your envato items for use as categories in new ticket and tickets list pages.\":\"This will automatically import all your envato items for use as categories in new ticket and tickets list pages.\",\"Import Envato Items\":\"Import Envato Items\",\"Genre\":\"Genre\",\"Genres to display on homepage. If left empty, most popular genres will be used.\":\"Genres to display on homepage. If left empty, most popular genres will be used.\",\"+Genre Name\":\"+Genre Name\",\"Localization\":\"Localization\",\"Manage localization settings for the site.\":\"Manage localization settings for the site.\",\"Timezone\":\"Timezone\",\"List of supported timezones can be found\":\"List of supported timezones can be found\",\"here.\":\"here.\",\"Translations Locale\":\"Translations Locale\",\"Date Locale\":\"Date Locale\",\"Date Format\":\"Date Format\",\"Default format for dates on the site. More information can be found\":\"Default format for dates on the site. More information can be found\",\"Enable translations functionality for the site.\":\"Enable translations functionality for the site.\",\"Configure site error and access logging and related 3rd party integrations.\":\"Configure site error and access logging and related 3rd party integrations.\",\"Sentry Public Key\":\"Sentry Public Key\",\"Sentry Private Key (DSN)\":\"Sentry Private Key (DSN)\",\"Information\":\"Information\",\"integration provides real-time error tracking and helps identify and fix issues when site is in production.\":\"integration provides real-time error tracking and helps identify and fix issues when site is in production.\",\"Change incoming and outgoing email handlers, email credentials and more.\":\"Change incoming and outgoing email handlers, email credentials and more.\",\"From Address\":\"From Address\",\"All outgoing application emails will be sent from this email address.\":\"All outgoing application emails will be sent from this email address.\",\"From Name\":\"From Name\",\"All outgoing application emails will be sent using this name.\":\"All outgoing application emails will be sent using this name.\",\"Your selected mail method must be authorized to send emails using this address and name.\":\"Your selected mail method must be authorized to send emails using this address and name.\",\"Outgoing Mail Method\":\"Outgoing Mail Method\",\"Which method should be used for sending outgoing application emails.\":\"Which method should be used for sending outgoing application emails.\",\"SMTP Host\":\"SMTP Host\",\"SMTP Username\":\"SMTP Username\",\"SMTP Password\":\"SMTP Password\",\"SMTP Port\":\"SMTP Port\",\"SMTP Encryption\":\"SMTP Encryption\",\"Mailgun Domain\":\"Mailgun Domain\",\"Usually the domain of your site (site.com)\":\"Usually the domain of your site (site.com)\",\"Mailgun Secret\":\"Mailgun Secret\",\"Should start with \\\"key-\\\"\":\"Should start with \\\"key-\\\"\",\"SES Key\":\"SES Key\",\"SES Secret\":\"SES Secret\",\"Sparkpost Secret\":\"Sparkpost Secret\",\"Default Mail Templates\":\"Default Mail Templates\",\"Use default email templates, even if they have been modified via \\\"Mail Templates\\\" page.\":\"Use default email templates, even if they have been modified via \\\"Mail Templates\\\" page.\",\"Permissions\":\"Permissions\",\"Player\":\"Player\",\"Select streaming method and configure player defaults and interface.\":\"Select streaming method and configure player defaults and interface.\",\"Streaming Method\":\"Streaming Method\",\"What method should be used to stream music on the site.\":\"What method should be used to stream music on the site.\",\"Youtube Api Key\":\"Youtube Api Key\",\"Suggested Quality\":\"Suggested Quality\",\"Small\":\"Small\",\"Medium\":\"Medium\",\"Large\":\"Large\",\"HD720\":\"HD720\",\"HD1080\":\"HD1080\",\"Highres\":\"Highres\",\"What quality should be used for youtube videos.\":\"What quality should be used for youtube videos.\",\"Youtube Region Code\":\"Youtube Region Code\",\"For what country should youtube results be returned. US recommended. ISO 3166-1 alpha-2 Code.\":\"For what country should youtube results be returned. US recommended. ISO 3166-1 alpha-2 Code.\",\"Soundcloud Api Key\":\"Soundcloud Api Key\",\"Default Player Volume (1 to 100)\":\"Default Player Volume (1 to 100)\",\"Custom Theme\":\"Custom Theme\",\"Enable custom theme (generated via appearance editor).\":\"Enable custom theme (generated via appearance editor).\",\"Hide Queue\":\"Hide Queue\",\"Should player queue (right sidebar) be hidden by default.\":\"Should player queue (right sidebar) be hidden by default.\",\"Hide Video\":\"Hide Video\",\"Should small video in the bottom right corner be hidden.\":\"Should small video in the bottom right corner be hidden.\",\"Hide Lyrics Button\":\"Hide Lyrics Button\",\"Hide \\\"Lyrics\\\" button in player controls bar.\":\"Hide \\\"Lyrics\\\" button in player controls bar.\",\"Hide Video Button\":\"Hide Video Button\",\"Hide video toggle button in player controls bar.\":\"Hide video toggle button in player controls bar.\",\"Data Providers\":\"Data Providers\",\"Select and configure providers that will be used to automate the site.\":\"Select and configure providers that will be used to automate the site.\",\"Local provider means that only data created manually via admin area will be used. No attempt will be made to fetch it from any 3rd party APIs or sites.\":\"Local provider means that only data created manually via admin area will be used. No attempt will be made to fetch it from any 3rd party APIs or sites.\",\"Generate Sitemap\":\"Generate Sitemap\",\"Artist Provider\":\"Artist Provider\",\"Album Provider\":\"Album Provider\",\"Search Provider\":\"Search Provider\",\"Genres Provider\":\"Genres Provider\",\"New Releases Provider\":\"New Releases Provider\",\"Popular Albums Provider\":\"Popular Albums Provider\",\"Top 50 Provider\":\"Top 50 Provider\",\"Artist Biography Provider\":\"Artist Biography Provider\",\"Wikipedia Language\":\"Wikipedia Language\",\"ISO 639-1 (two letter) language code.\":\"ISO 639-1 (two letter) language code.\",\"Spotify ID\":\"Spotify ID\",\"Spotify Secret\":\"Spotify Secret\",\"Discogs ID\":\"Discogs ID\",\"Discogs Secret\":\"Discogs Secret\",\"Last.fm Api Key\":\"Last.fm Api Key\",\"Enable Certificate Verification\":\"Enable Certificate Verification\",\"Should only be disabled if there are problems with 3rd party data providers.\":\"Should only be disabled if there are problems with 3rd party data providers.\",\"Select active queue method and enter related 3rd party API keys.\":\"Select active queue method and enter related 3rd party API keys.\",\"Queues allow to defer time consuming tasks, such as sending an email, until a later time. Deferring these tasks can speed up web requests to the application.\":\"Queues allow to defer time consuming tasks, such as sending an email, until a later time. Deferring these tasks can speed up web requests to the application.\",\"Important\":\"Important\",\"All methods except sync require additional setup, which should be performed before changing the queue method. Consult documentation for more information.\":\"All methods except sync require additional setup, which should be performed before changing the queue method. Consult documentation for more information.\",\"Queue Method\":\"Queue Method\",\"SQS Queue Key\":\"SQS Queue Key\",\"SQS Queue Secret\":\"SQS Queue Secret\",\"SQS Queue Prefix\":\"SQS Queue Prefix\",\"SQS Queue Name\":\"SQS Queue Name\",\"SQS Queue Region\":\"SQS Queue Region\",\"Mail\":\"Mail\",\"Logging\":\"Logging\",\"Queue\":\"Queue\",\"Blocked Artists\":\"Blocked Artists\",\"Update Track\\n            New Track\":\"Update Track\\n            New Track\",\"Update Track\":\"Update Track\",\"Album Name\":\"Album Name\",\"Duration (ms)\":\"Duration (ms)\",\"Popularity (1 to 100)\":\"Popularity (1 to 100)\",\"Youtube ID\":\"Youtube ID\",\"Url\":\"Url\",\"Upload Music or Video\":\"Upload Music or Video\",\"Edit Lyrics\":\"Edit Lyrics\",\"Seems like no tracks have been created yet.\":\"Seems like no tracks have been created yet.\",\"Update Localization\":\"Update Localization\",\"Create New Localization\":\"Create New Localization\",\"Localization Name\":\"Localization Name\",\"New Localization\":\"New Localization\",\"Set as Default\":\"Set as Default\",\"Rename\":\"Rename\",\"Search Translations\":\"Search Translations\",\"Source Text\":\"Source Text\",\"Translation\":\"Translation\",\"Update User Details\":\"Update User Details\",\"Create a New User\":\"Create a New User\",\"Password\":\"Password\",\"Update User\":\"Update User\",\"Create User\":\"Create User\",\"Select Groups\":\"Select Groups\",\"Select\":\"Select\",\"Select Permissions\":\"Select Permissions\",\"User Groups\":\"User Groups\",\"No groups yet.\":\"No groups yet.\",\"User Permissions\":\"User Permissions\",\"Users Selected\":\"Users Selected\",\"Edit User\":\"Edit User\",\"Seems like no users have been created yet.\":\"Seems like no users have been created yet.\",\"Forgot Password?\":\"Forgot Password?\",\"Enter your email address below and we will send you a link to reset it.\":\"Enter your email address below and we will send you a link to reset it.\",\"Back\":\"Back\",\"Send\":\"Send\",\"Forgot?\":\"Forgot?\",\"Remember Me\":\"Remember Me\",\"Login\":\"Login\",\"Don\'t have an account?\":\"Don\'t have an account?\",\"Register here.\":\"Register here.\",\"Login With Envato\":\"Login With Envato\",\"Login with facebook\":\"Login with facebook\",\"Login with google\":\"Login with google\",\"Login with twitter\":\"Login with twitter\",\"Confirm Password\":\"Confirm Password\",\"Register\":\"Register\",\"Already have an account?\":\"Already have an account?\",\"Login in here.\":\"Login in here.\",\"Credentials Required\":\"Credentials Required\",\"Email Address\":\"Email Address\",\"We need your email address to create an account for you, please enter it above.\":\"We need your email address to create an account for you, please enter it above.\",\"An account with this email address already exists, if you want to connect the two accounts please enter existing accounts password above.\":\"An account with this email address already exists, if you want to connect the two accounts please enter existing accounts password above.\",\"Connect\":\"Connect\",\"New Password\":\"New Password\",\"Confirm New Password\":\"Confirm New Password\",\"Reset Password\":\"Reset Password\",\"Upload File\":\"Upload File\",\"Upload\":\"Upload\",\"Drop Files Here or Click to Upload\":\"Drop Files Here or Click to Upload\",\"It must be direct link to a file.  Example: http:\\/\\/site.com\\/image.png\":\"It must be direct link to a file.  Example: http:\\/\\/site.com\\/image.png\",\"Add Attachment\":\"Add Attachment\",\"Insert Code Sample\":\"Insert Code Sample\",\"Undo\":\"Undo\",\"Redo\":\"Redo\",\"Bold\":\"Bold\",\"Italic\":\"Italic\",\"Underline\":\"Underline\",\"Unordered List\":\"Unordered List\",\"Ordered List\":\"Ordered List\",\"Insert Link\":\"Insert Link\",\"Insert Image\":\"Insert Image\",\"Remove Formatting\":\"Remove Formatting\",\"Warning\":\"Warning\",\"Note\":\"Note\",\"Visual\":\"Visual\",\"Source\":\"Source\",\"Release Date:\":\"Release Date:\",\"Number of Songs:\":\"Number of Songs:\",\"Running Time:\":\"Running Time:\",\"Play\":\"Play\",\"Pause\":\"Pause\",\"More...\":\"More...\",\"Add to Queue\":\"Add to Queue\",\"Add to Playlist\":\"Add to Playlist\",\"Save to Your Music\":\"Save to Your Music\",\"Copy Album Link\":\"Copy Album Link\",\"Share\":\"Share\",\"New Releases\":\"New Releases\",\"No new releases found.\":\"No new releases found.\",\"Could not find any new releases. Please try again later.\":\"Could not find any new releases. Please try again later.\",\"Popular Albums\":\"Popular Albums\",\"No albums found.\":\"No albums found.\",\"Could not find any popular albums. Please try again later.\":\"Could not find any popular albums. Please try again later.\",\"Overview\":\"Overview\",\"Similar Artists\":\"Similar Artists\",\"About\":\"About\",\"Popular Songs\":\"Popular Songs\",\"Show More\":\"Show More\",\"Show Less\":\"Show Less\",\"Could not find any similar artists for\":\"Could not find any similar artists for\",\"Could not find biography for\":\"Could not find biography for\",\"Go to Artist Radio\":\"Go to Artist Radio\",\"Copy Artist Link\":\"Copy Artist Link\",\"{{playlist.name}}\":\"{{playlist.name}}\",\"Copy\":\"Copy\",\"via email\":\"via email\",\"+Email address\":\"+Email address\",\"Sharing with:\":\"Sharing with:\",\"Remove address\":\"Remove address\",\"Filter...\":\"Filter...\",\"Sorted by\":\"Sorted by\",\"Minimize\":\"Minimize\",\"Show Queue\":\"Show Queue\",\"Show video\":\"Show video\",\"Fullscreen\":\"Fullscreen\",\"{{genre.name}} Artists\":\"{{genre.name}} Artists\",\"Popular Genres\":\"Popular Genres\",\"No genres found.\":\"No genres found.\",\"Could not find any popular genres. Please try again later.\":\"Could not find any popular genres. Please try again later.\",\"Main site search\":\"Main site search\",\"Search...\":\"Search...\",\"New Artist\":\"New Artist\",\"Your Music\":\"Your Music\",\"Toggle video\":\"Toggle video\",\"Toggle queue\":\"Toggle queue\",\"Shuffle\":\"Shuffle\",\"Repeat\":\"Repeat\",\"Repeat One\":\"Repeat One\",\"Mute\":\"Mute\",\"Unmute\":\"Unmute\",\"Update Playlist\":\"Update Playlist\",\"Playlist Name\":\"Playlist Name\",\"Playlist Description\":\"Playlist Description\",\"Give your playlist a catchy description.\":\"Give your playlist a catchy description.\",\"Songs\":\"Songs\",\"Follow\":\"Follow\",\"Unfollow\":\"Unfollow\",\"Make Public\":\"Make Public\",\"Make Private\":\"Make Private\",\"Copy Link\":\"Copy Link\",\"By\":\"By\",\"Go to Track Radio\":\"Go to Track Radio\",\"Remove from Queue\":\"Remove from Queue\",\"Remove from Playlist\":\"Remove from Playlist\",\"Remove from Your Music\":\"Remove from Your Music\",\"Show Lyrics\":\"Show Lyrics\",\"Copy Track Link\":\"Copy Track Link\",\"Radio\":\"Radio\",\"Search results for\":\"Search results for\",\"Top Results\":\"Top Results\",\"No results for\":\"No results for\",\"Please check your spelling or try using different keywords.\":\"Please check your spelling or try using different keywords.\",\"Search\":\"Search\",\"Find artists, albums, songs, playlists and more.\":\"Find artists, albums, songs, playlists and more.\",\"View All Results...\":\"View All Results...\",\"Followers\":\"Followers\",\"No results found.\":\"No results found.\",\"Top 50\":\"Top 50\",\"No charts found.\":\"No charts found.\",\"Could not find charts. Please try again later.\":\"Could not find charts. Please try again later.\",\"Duration:\":\"Duration:\",\"Featured in\":\"Featured in\",\"Update Name or Profile Image\":\"Update Name or Profile Image\",\"Profile image\":\"Profile image\",\"For best results, upload a high resolution image\":\"For best results, upload a high resolution image\",\"Upload Image\":\"Upload Image\",\"Remove Image\":\"Remove Image\",\"Save Changes\":\"Save Changes\",\"Manage Social Logins\":\"Manage Social Logins\",\"Enable or disable connected social services\":\"Enable or disable connected social services\",\"Update Password\":\"Update Password\",\"Current Password\":\"Current Password\",\"Update Account Preferences\":\"Update Account Preferences\",\"Language\":\"Language\",\"Country\":\"Country\",\"Google+ Account\":\"Google+ Account\",\"Disable\":\"Disable\",\"Enable\":\"Enable\",\"Facebook Account\":\"Facebook Account\",\"Twitter Account\":\"Twitter Account\",\"Date Added\":\"Date Added\",\"Artist Name\":\"Artist Name\",\"Nothing to display.\":\"Nothing to display.\",\"You have not added any albums to your library yet.\":\"You have not added any albums to your library yet.\",\"Number of Songs\":\"Number of Songs\",\"You have not added any artists to your library yet.\":\"You have not added any artists to your library yet.\",\"You have not added any songs to your library yet.\":\"You have not added any songs to your library yet.\",\"Public Playlists\":\"Public Playlists\",\"Following\":\"Following\",\"Seems like this user has not created any playlists yet.\":\"Seems like this user has not created any playlists yet.\",\"Seems like this user is not following anyone yet.\":\"Seems like this user is not following anyone yet.\",\"Seems like no one is following this user yet.\":\"Seems like no one is following this user yet.\",\"Ads have been updated.\":\"Ads have been updated.\",\"Delete Albums\":\"Delete Albums\",\"Are you sure you want to delete selected albums?\":\"Are you sure you want to delete selected albums?\",\"Delete Tracks\":\"Delete Tracks\",\"Are you sure you want to delete selected tracks?\":\"Are you sure you want to delete selected tracks?\",\"Appearance Saved.\":\"Appearance Saved.\",\"Close Appearance Editor\":\"Close Appearance Editor\",\"Are you sure you want to close appearance editor?\":\"Are you sure you want to close appearance editor?\",\"All unsaved changes will be lost.\":\"All unsaved changes will be lost.\",\"Stay\":\"Stay\",\"Delete Menu Item\":\"Delete Menu Item\",\"Are you sure you want to delete this menu item?\":\"Are you sure you want to delete this menu item?\",\"Are you sure you want to delete this menu?\":\"Are you sure you want to delete this menu?\",\"Delete Artists\":\"Delete Artists\",\"Are you sure you want to delete selected artists?\":\"Are you sure you want to delete selected artists?\",\"Artist created.\":\"Artist created.\",\"Artist updated.\":\"Artist updated.\",\"Users assigned to group.\":\"Users assigned to group.\",\"Users removed from group.\":\"Users removed from group.\",\"Delete Group\":\"Delete Group\",\"Are you sure you want to delete this group?\":\"Are you sure you want to delete this group?\",\"Remove Users from Group\":\"Remove Users from Group\",\"Are you sure you want to remove selected users from this group?\":\"Are you sure you want to remove selected users from this group?\",\"Delete Lyrics\":\"Delete Lyrics\",\"Are you sure you want to delete selected lyrics?\":\"Are you sure you want to delete selected lyrics?\",\"Mail template updated\":\"Mail template updated\",\"Page Created\":\"Page Created\",\"Page Updated\":\"Page Updated\",\"Delete Pages\":\"Delete Pages\",\"Are you sure you want to delete selected pages?\":\"Are you sure you want to delete selected pages?\",\"Delete Playlists\":\"Delete Playlists\",\"Are you sure you want to delete selected playlists?\":\"Are you sure you want to delete selected playlists?\",\"Saved settings\":\"Saved settings\",\"Cache cleared.\":\"Cache cleared.\",\"Imported envato items\":\"Imported envato items\",\"Sitemap generated.\":\"Sitemap generated.\",\"Default Localization Changed\":\"Default Localization Changed\",\"Localizations Updated\":\"Localizations Updated\",\"Delete Localization\":\"Delete Localization\",\"Are you sure you want to delete this localization?\":\"Are you sure you want to delete this localization?\",\"Localization Deleted\":\"Localization Deleted\",\"Delete Users\":\"Delete Users\",\"Are you sure you want to delete selected users?\":\"Are you sure you want to delete selected users?\",\"We have sent you an email with instructions on how to activate your account.\":\"We have sent you an email with instructions on how to activate your account.\",\"Your password has been reset.\":\"Your password has been reset.\",\"This album has no songs yet.\":\"This album has no songs yet.\",\"Copied link to clipboard.\":\"Copied link to clipboard.\",\"Could not find lyrics for this song.\":\"Could not find lyrics for this song.\",\"Delete Playlist\":\"Delete Playlist\",\"Are you sure you want to delete this playlist?\":\"Are you sure you want to delete this playlist?\",\"Account settings updated\":\"Account settings updated\",\"Avatar updated\":\"Avatar updated\",\"Avatar removed\":\"Avatar removed\",\"Password updated\":\"Password updated\",\"You have already created a playlist with this name.\":\"You have already created a playlist with this name.\",\"auth.failed\":\"These credentials do not match our records.\",\"auth.throttle\":\"Too many login attempts. Please try again in :seconds seconds.\",\"pagination.previous\":\"&laquo; Previous\",\"pagination.next\":\"Next &raquo;\",\"passwords.password\":\"Passwords must be at least six characters and match the confirmation.\",\"passwords.reset\":\"Your password has been reset!\",\"passwords.sent\":\"We have e-mailed your password reset link!\",\"passwords.token\":\"This password reset token is invalid.\",\"passwords.user\":\"We can\'t find a user with that e-mail address.\",\"validation.accepted\":\"The :attribute must be accepted.\",\"validation.active_url\":\"The :attribute is not a valid URL.\",\"validation.after\":\"The :attribute must be a date after :date.\",\"validation.after_or_equal\":\"The :attribute must be a date after or equal to :date.\",\"validation.alpha\":\"The :attribute may only contain letters.\",\"validation.alpha_dash\":\"The :attribute may only contain letters, numbers, and dashes.\",\"validation.alpha_num\":\"The :attribute may only contain letters and numbers.\",\"validation.array\":\"The :attribute must be an array.\",\"validation.before\":\"The :attribute must be a date before :date.\",\"validation.before_or_equal\":\"The :attribute must be a date before or equal to :date.\",\"validation.between.numeric\":\"The :attribute must be between :min and :max.\",\"validation.between.file\":\"The :attribute must be between :min and :max kilobytes.\",\"validation.between.string\":\"The :attribute must be between :min and :max characters.\",\"validation.between.array\":\"The :attribute must have between :min and :max items.\",\"validation.boolean\":\"The :attribute field must be true or false.\",\"validation.confirmed\":\"The :attribute confirmation does not match.\",\"validation.date\":\"The :attribute is not a valid date.\",\"validation.date_format\":\"The :attribute does not match the format :format.\",\"validation.different\":\"The :attribute and :other must be different.\",\"validation.digits\":\"The :attribute must be :digits digits.\",\"validation.digits_between\":\"The :attribute must be between :min and :max digits.\",\"validation.dimensions\":\"The :attribute has invalid image dimensions.\",\"validation.distinct\":\"The :attribute field has a duplicate value.\",\"validation.email\":\"The :attribute must be a valid email address.\",\"validation.exists\":\"The selected :attribute is invalid.\",\"validation.file\":\"The :attribute must be a file.\",\"validation.filled\":\"The :attribute field must have a value.\",\"validation.image\":\"The :attribute must be an image.\",\"validation.in\":\"The selected :attribute is invalid.\",\"validation.in_array\":\"The :attribute field does not exist in :other.\",\"validation.integer\":\"The :attribute must be an integer.\",\"validation.ip\":\"The :attribute must be a valid IP address.\",\"validation.ipv4\":\"The :attribute must be a valid IPv4 address.\",\"validation.ipv6\":\"The :attribute must be a valid IPv6 address.\",\"validation.json\":\"The :attribute must be a valid JSON string.\",\"validation.max.numeric\":\"The :attribute may not be greater than :max.\",\"validation.max.file\":\"The :attribute may not be greater than :max kilobytes.\",\"validation.max.string\":\"The :attribute may not be greater than :max characters.\",\"validation.max.array\":\"The :attribute may not have more than :max items.\",\"validation.mimes\":\"The :attribute must be a file of type: :values.\",\"validation.mimetypes\":\"The :attribute must be a file of type: :values.\",\"validation.min.numeric\":\"The :attribute must be at least :min.\",\"validation.min.file\":\"The :attribute must be at least :min kilobytes.\",\"validation.min.string\":\"The :attribute must be at least :min characters.\",\"validation.min.array\":\"The :attribute must have at least :min items.\",\"validation.not_in\":\"The selected :attribute is invalid.\",\"validation.numeric\":\"The :attribute must be a number.\",\"validation.present\":\"The :attribute field must be present.\",\"validation.regex\":\"The :attribute format is invalid.\",\"validation.required\":\"The :attribute field is required.\",\"validation.required_if\":\"The :attribute field is required when :other is :value.\",\"validation.required_unless\":\"The :attribute field is required unless :other is in :values.\",\"validation.required_with\":\"The :attribute field is required when :values is present.\",\"validation.required_with_all\":\"The :attribute field is required when :values is present.\",\"validation.required_without\":\"The :attribute field is required when :values is not present.\",\"validation.required_without_all\":\"The :attribute field is required when none of :values are present.\",\"validation.same\":\"The :attribute and :other must match.\",\"validation.size.numeric\":\"The :attribute must be :size.\",\"validation.size.file\":\"The :attribute must be :size kilobytes.\",\"validation.size.string\":\"The :attribute must be :size characters.\",\"validation.size.array\":\"The :attribute must contain :size items.\",\"validation.string\":\"The :attribute must be a string.\",\"validation.timezone\":\"The :attribute must be a valid zone.\",\"validation.unique\":\"The :attribute has already been taken.\",\"validation.uploaded\":\"The :attribute failed to upload.\",\"validation.url\":\"The :attribute format is invalid.\",\"validation.email_confirmed\":\"This email address is not confirmed yet.\"}','2018-03-20 15:19:06','2018-03-20 15:19:06');
/*!40000 ALTER TABLE `localizations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `lyrics`
--

DROP TABLE IF EXISTS `lyrics`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `lyrics` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `text` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `track_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `lyrics_track_id_unique` (`track_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `lyrics`
--

LOCK TABLES `lyrics` WRITE;
/*!40000 ALTER TABLE `lyrics` DISABLE KEYS */;
/*!40000 ALTER TABLE `lyrics` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `mail_templates`
--

DROP TABLE IF EXISTS `mail_templates`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mail_templates` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `display_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `file_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `subject` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `markdown` tinyint(1) NOT NULL DEFAULT '0',
  `action` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `mail_templates_file_name_unique` (`file_name`),
  UNIQUE KEY `mail_templates_action_unique` (`action`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `mail_templates`
--

LOCK TABLES `mail_templates` WRITE;
/*!40000 ALTER TABLE `mail_templates` DISABLE KEYS */;
INSERT INTO `mail_templates` VALUES (1,'Generic','generic.blade.php','{{EMAIL_SUBJECT}}',0,'generic','2018-03-20 15:19:05','2018-03-20 15:19:05'),(2,'Email Confirmation','email-confirmation.blade.php','Confirm your {{SITE_NAME}} account',0,'email_confirmation','2018-03-20 15:19:05','2018-03-20 15:19:06'),(3,'Share','share.blade.php','{{DISPLAY_NAME}} shared \'{{ITEM_NAME}}\' with you',0,'share','2018-03-20 15:19:06','2018-03-20 15:19:06');
/*!40000 ALTER TABLE `mail_templates` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `migrations`
--

DROP TABLE IF EXISTS `migrations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=58 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `migrations`
--

LOCK TABLES `migrations` WRITE;
/*!40000 ALTER TABLE `migrations` DISABLE KEYS */;
INSERT INTO `migrations` VALUES (1,'2014_10_12_000000_create_users_table',1),(2,'2014_10_12_100000_create_password_resets_table',1),(3,'2015_04_127_156842_create_social_profiles_table',1),(4,'2015_04_127_156842_create_users_oauth_table',1),(5,'2015_05_29_131549_create_settings_table',1),(6,'2015_09_04_155015_create_artists_table',1),(7,'2015_09_06_161342_create_albums_table',1),(8,'2015_09_06_161348_create_tracks_table',1),(9,'2015_09_11_145318_create_similar_artists_table',1),(10,'2015_09_17_135717_create_track_user_table',1),(11,'2015_09_26_124652_create_playlists_table',1),(12,'2015_09_26_131215_create_playlist_track_table',1),(13,'2015_09_26_135719_create_playlist_user_table',1),(14,'2015_10_16_135253_create_genres_table',1),(15,'2015_10_16_135754_create_genre_artist',1),(16,'2015_10_23_164355_create_follows_table',1),(17,'2015_11_18_134303_add_temp_id_to_albums',1),(18,'2015_11_18_134303_add_temp_id_to_tracks',1),(19,'2015_11_19_134203_change_fully_scraped_default',1),(20,'2016_03_03_143235_add_position_to_playlist_track_table',1),(21,'2016_03_14_143858_add_url_to_tracks_table',1),(22,'2016_03_18_142141_add_email_confirmation_to_users_table',1),(23,'2016_03_19_140502_add_bio_field_to_artists',1),(24,'2016_03_24_148503_add_fully_scraped_index_to_albums_table',1),(25,'2016_03_24_148503_add_fully_scraped_index_to_artists_table',1),(26,'2016_03_24_148503_add_public_index_to_playlists_table',1),(27,'2016_03_25_174157_create_sitemap_ids_table',1),(28,'2016_03_28_150334_add_image_and_description_to_playlists_table',1),(29,'2016_04_10_150419_add_wiki_images_to_artists_table',1),(30,'2016_05_02_150429_change_artists_fully_scraped_default',1),(31,'2016_05_26_170044_create_uploads_table',1),(32,'2016_07_14_153703_create_groups_table',1),(33,'2016_07_14_153921_create_user_group_table',1),(34,'2017_07_02_120142_create_pages_table',1),(35,'2017_07_11_122825_create_localizations_table',1),(36,'2017_07_17_135837_create_mail_templates_table',1),(37,'2017_08_26_131330_add_private_field_to_settings_table',1),(38,'2017_08_26_155115_add_timestamps_to_artists_table',1),(39,'2017_09_12_134214_set_playlist_user_owner_column_default_to_zero',1),(40,'2017_09_16_155557_create_lyrics_table',1),(41,'2017_09_17_144728_add_columns_to_users_table',1),(42,'2017_09_17_152854_make_password_column_nullable',1),(43,'2017_09_17_152854_rename_avatar_url_column',1),(44,'2017_09_30_152855_make_settings_value_column_nullable',1),(45,'2017_10_01_152856_add_views_column_to_artists_table',1),(46,'2017_10_01_152857_add_views_column_to_albums_table',1),(47,'2017_10_01_152858_add_plays_column_to_tracks_table',1),(48,'2017_10_01_152859_add_views_column_to_playlists_table',1),(49,'2018_02_23_172719_create_aggregators_table',1),(50,'2018_03_02_102305_drop_spotify_popularity_column',1),(51,'2018_03_02_103027_create_aggregated_artists',1),(52,'2018_03_02_104010_drop_albums_spotify_popularity_column',1),(53,'2018_03_02_104527_drop_tracks_spotify_popularity_column',1),(54,'2018_03_02_113944_drop_spotify_followers_column',1),(55,'2018_03_02_120758_add_username_artists_table',1),(57,'2018_03_22_110325_add_artist_id_tracks_table',2);
/*!40000 ALTER TABLE `migrations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pages`
--

DROP TABLE IF EXISTS `pages`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pages` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `body` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `pages_slug_unique` (`slug`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pages`
--

LOCK TABLES `pages` WRITE;
/*!40000 ALTER TABLE `pages` DISABLE KEYS */;
/*!40000 ALTER TABLE `pages` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `playlist_track`
--

DROP TABLE IF EXISTS `playlist_track`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `playlist_track` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `playlist_id` int(10) unsigned NOT NULL,
  `track_id` int(10) unsigned NOT NULL,
  `position` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  UNIQUE KEY `playlist_track_track_id_playlist_id_unique` (`track_id`,`playlist_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `playlist_track`
--

LOCK TABLES `playlist_track` WRITE;
/*!40000 ALTER TABLE `playlist_track` DISABLE KEYS */;
INSERT INTO `playlist_track` VALUES (1,1,3,1);
/*!40000 ALTER TABLE `playlist_track` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `playlist_user_info`
--

DROP TABLE IF EXISTS `playlist_user_info`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `playlist_user_info` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `playlist_id` int(10) unsigned NOT NULL,
  `user_info_id` int(10) unsigned NOT NULL,
  `owner` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `playlist_user_info_playlist_id_user_info_id_unique` (`playlist_id`,`user_info_id`),
  KEY `playlist_user_info_owner_index` (`owner`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `playlist_user_info`
--

LOCK TABLES `playlist_user_info` WRITE;
/*!40000 ALTER TABLE `playlist_user_info` DISABLE KEYS */;
INSERT INTO `playlist_user_info` VALUES (1,1,18,1);
/*!40000 ALTER TABLE `playlist_user_info` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `playlists`
--

DROP TABLE IF EXISTS `playlists`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `playlists` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `public` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `views` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `playlists_public_index` (`public`),
  KEY `playlists_views_index` (`views`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `playlists`
--

LOCK TABLES `playlists` WRITE;
/*!40000 ALTER TABLE `playlists` DISABLE KEYS */;
INSERT INTO `playlists` VALUES (1,'Ocean',1,'2018-03-22 13:59:52','2018-03-22 13:59:52','http://localhost:8010/assets/images/default/artist_small.jpg','Drive thru',1);
/*!40000 ALTER TABLE `playlists` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `settings`
--

DROP TABLE IF EXISTS `settings`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `settings` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `value` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `private` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `settings_name_unique` (`name`),
  KEY `settings_private_index` (`private`)
) ENGINE=InnoDB AUTO_INCREMENT=68 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `settings`
--

LOCK TABLES `settings` WRITE;
/*!40000 ALTER TABLE `settings` DISABLE KEYS */;
INSERT INTO `settings` VALUES (1,'dates.format','%b %e, %H:%M','2018-03-20 15:19:03','2018-03-20 15:19:03',0),(2,'dates.locale','en_US','2018-03-20 15:19:03','2018-03-20 15:19:03',0),(3,'social.google.enable','1','2018-03-20 15:19:03','2018-03-20 15:19:03',0),(4,'social.twitter.enable','1','2018-03-20 15:19:03','2018-03-20 15:19:03',0),(5,'social.facebook.enable','1','2018-03-20 15:19:03','2018-03-20 15:19:03',0),(6,'realtime.enable','0','2018-03-20 15:19:03','2018-03-20 15:19:03',0),(7,'registration.disable','0','2018-03-20 15:19:03','2018-03-20 15:19:03',0),(8,'cache.report_minutes','60','2018-03-20 15:19:03','2018-03-20 15:19:03',0),(9,'cache.homepage_days','1','2018-03-20 15:19:03','2018-03-20 15:19:03',0),(10,'automation.artist_interval','7','2018-03-20 15:19:03','2018-03-20 15:19:03',0),(11,'branding.use_custom_theme','1','2018-03-20 15:19:03','2018-03-22 14:30:36',0),(12,'branding.site_logo','http://localhost:8010/storage/branding_images/p8c6xCxeMugTQAcbI7zpMsseXHy4uUfgNc7c7huI.png','2018-03-20 15:19:03','2018-03-22 14:30:35',0),(13,'branding.site_name','737Music','2018-03-20 15:19:03','2018-03-22 14:30:36',0),(14,'branding.favicon','http://localhost:8010/storage/branding_images/V1iqNHTGQkkGYgGrUzXTJGO9iGCwiwxro4rxwltG.png','2018-03-20 15:19:03','2018-03-22 14:30:36',0),(15,'i18n.default_localization','English','2018-03-20 15:19:03','2018-03-20 15:19:03',0),(16,'i18n.enable','1','2018-03-20 15:19:03','2018-03-20 15:19:03',0),(17,'homepage.type','default','2018-03-20 15:19:03','2018-03-20 15:19:03',0),(18,'homepage.value','popular-genres','2018-03-20 15:19:03','2018-03-20 15:19:03',0),(19,'seo.artist_title','Listen to {{ARTIST_NAME}}','2018-03-20 15:19:03','2018-03-20 15:19:03',0),(20,'seo.artist_description','some stuff {{ARTIST_DESCRIPTION}}','2018-03-20 15:19:03','2018-03-20 15:19:03',0),(21,'seo.album_title','{{ALBUM_NAME}}','2018-03-20 15:19:03','2018-03-20 15:19:03',0),(22,'seo.album_description','{{ALBUM_NAME}} album by {{ARTIST_NAME}} on {{SITE_NAME}}','2018-03-20 15:19:03','2018-03-20 15:19:03',0),(23,'seo.track_title','{{TRACK_NAME}}','2018-03-20 15:19:03','2018-03-20 15:19:03',0),(24,'seo.track_description','{{TRACK_NAME}}, a song by {{ARTIST_NAME}} on {{SITE_NAME}}','2018-03-20 15:19:03','2018-03-20 15:19:03',0),(25,'seo.playlist_title','{{PLAYLIST_NAME}}, playlist by {{CREATOR_NAME}} on {{SITE_NAME}}','2018-03-20 15:19:03','2018-03-20 15:19:03',0),(26,'seo.playlist_description','{{PLAYLIST_DESCRIPTION}}','2018-03-20 15:19:03','2018-03-20 15:19:03',0),(27,'seo.user_profile_title','{{DISPLAY_NAME}}','2018-03-20 15:19:03','2018-03-20 15:19:03',0),(28,'seo.user_profile_description','{{DISPLAY_NAME}} profile on {{SITE_NAME}}','2018-03-20 15:19:03','2018-03-20 15:19:03',0),(29,'seo.search_title','Search results for {{QUERY}}','2018-03-20 15:19:03','2018-03-20 15:19:03',0),(30,'seo.search_description','Search results for {{QUERY}}','2018-03-20 15:19:03','2018-03-20 15:19:03',0),(31,'seo.genre_title','{{GENRE_NAME}}','2018-03-20 15:19:03','2018-03-20 15:19:03',0),(32,'seo.genre_description','{{GENRE_DESCRIPTION}}','2018-03-20 15:19:03','2018-03-20 15:19:03',0),(33,'seo.new_releases_title','Latest releases','2018-03-20 15:19:03','2018-03-20 15:19:03',0),(34,'seo.new_releases_description','Browse and listen to newest releases from popular artists.','2018-03-20 15:19:03','2018-03-20 15:19:03',0),(35,'seo.popular_genres_title','Popular Genres','2018-03-20 15:19:03','2018-03-20 15:19:03',0),(36,'seo.popular_genres_description','Browse popular genres to discover new music.','2018-03-20 15:19:03','2018-03-20 15:19:03',0),(37,'seo.popular_albums_title','Popular Albums','2018-03-20 15:19:03','2018-03-20 15:19:03',0),(38,'seo.popular_albums_description','Most popular albums from hottest artists today.','2018-03-20 15:19:03','2018-03-20 15:19:03',0),(39,'seo.top_50_title','Top 50 Tracks','2018-03-20 15:19:03','2018-03-20 15:19:03',0),(40,'seo.top_50_description','Global Top 50 chart of most popular songs.','2018-03-20 15:19:03','2018-03-20 15:19:03',0),(41,'seo.home_title','BeMusic - Listen to music for free.','2018-03-20 15:19:03','2018-03-20 15:19:03',0),(42,'seo.home_description','Find and listen to millions of songs, albums and artists, all completely free on BeMusic.','2018-03-20 15:19:03','2018-03-20 15:19:03',0),(43,'logging.sentry_public',NULL,'2018-03-20 15:19:03','2018-03-20 15:19:03',0),(44,'realtime.pusher_key',NULL,'2018-03-20 15:19:03','2018-03-20 15:19:03',0),(45,'menus','[{\"name\":\"Primary\",\"position\":\"sidebar-primary\",\"items\":[{\"type\":\"route\",\"order\":1,\"label\":\"Popular Albums\",\"action\":\"popular-albums\",\"icon\":\"whatshot\"},{\"type\":\"route\",\"order\":2,\"label\":\"Popular Genres\",\"action\":\"popular-genres\",\"icon\":\"local-offer\"},{\"type\":\"route\",\"order\":3,\"label\":\"Top 50\",\"action\":\"top-50\",\"icon\":\"trending-up\"},{\"type\":\"route\",\"order\":4,\"label\":\"New Releases\",\"action\":\"new-releases\",\"icon\":\"album\"}]},{\"name\":\"Secondary \",\"position\":\"sidebar-secondary\",\"items\":[{\"type\":\"route\",\"order\":1,\"label\":\"Songs\",\"action\":\"\\/library\\/songs\",\"icon\":\"audiotrack\"},{\"type\":\"route\",\"order\":2,\"label\":\"Albums\",\"action\":\"\\/library\\/albums\",\"icon\":\"album\"},{\"type\":\"route\",\"order\":3,\"label\":\"Artists\",\"action\":\"\\/library\\/artists\",\"icon\":\"mic\"}]},{\"name\":\"Mobile \",\"position\":\"mobile-bottom\",\"items\":[{\"type\":\"route\",\"order\":1,\"label\":\"Genres\",\"action\":\"\\/popular-genres\",\"icon\":\"local-offer\"},{\"type\":\"route\",\"order\":2,\"label\":\"Top 50\",\"action\":\"\\/top-50\",\"icon\":\"trending-up\"},{\"type\":\"route\",\"order\":3,\"label\":\"Search\",\"action\":\"\\/search\",\"icon\":\"search\"},{\"type\":\"route\",\"order\":4,\"label\":\"Your Music\",\"action\":\"\\/library\",\"icon\":\"library-music\"},{\"type\":\"route\",\"order\":4,\"label\":\"Account\",\"action\":\"\\/account-settings\",\"icon\":\"person\"}]}]','2018-03-20 15:19:03','2018-03-20 15:19:04',0),(46,'artist_provider','Local','2018-03-20 15:19:03','2018-03-20 15:19:03',0),(47,'album_provider','Local','2018-03-20 15:19:03','2018-03-20 15:19:03',0),(48,'radio_provider','Spotify','2018-03-20 15:19:03','2018-03-20 15:19:03',0),(49,'genres_provider','Local','2018-03-20 15:19:03','2018-03-20 15:19:03',0),(50,'album_images_provider','real','2018-03-20 15:19:03','2018-03-20 15:19:03',0),(51,'artist_images_provider','real','2018-03-20 15:19:03','2018-03-20 15:19:03',0),(52,'new_releases_provider','Local','2018-03-20 15:19:03','2018-03-20 15:19:03',0),(53,'top_tracks_provider','Local','2018-03-20 15:19:03','2018-03-20 15:19:03',0),(54,'top_albums_provider','Local','2018-03-20 15:19:03','2018-03-20 15:19:03',0),(55,'search_provider','Local','2018-03-20 15:19:03','2018-03-20 15:19:03',0),(56,'audio_search_provider','Youtube','2018-03-20 15:19:03','2018-03-20 15:19:03',0),(57,'artist_bio_provider','wikipedia','2018-03-20 15:19:03','2018-03-20 15:19:03',0),(58,'youtube.suggested_quality','default','2018-03-20 15:19:03','2018-03-20 15:19:03',0),(59,'youtube.region_code','US','2018-03-20 15:19:03','2018-03-20 15:19:03',0),(60,'player.default_volume','30','2018-03-20 15:19:03','2018-03-20 15:19:03',0),(61,'player.hide_queue','0','2018-03-20 15:19:03','2018-03-20 15:19:03',0),(62,'player.hide_video','0','2018-03-20 15:19:03','2018-03-20 15:19:03',0),(63,'player.hide_video_button','0','2018-03-20 15:19:03','2018-03-20 15:19:03',0),(64,'player.hide_lyrics','0','2018-03-20 15:19:03','2018-03-20 15:19:03',0),(65,'player.mobile.auto_open_overlay','1','2018-03-20 15:19:03','2018-03-20 15:19:03',0),(66,'https.enable_cert_verification','1','2018-03-20 15:19:03','2018-03-20 15:19:03',0),(67,'homepage.genres','[\"Alternative\",\"Pop\",\"Rock\",\"Makosa\"]','2018-03-22 14:10:20','2018-03-22 14:10:20',0);
/*!40000 ALTER TABLE `settings` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `similar_artists`
--

DROP TABLE IF EXISTS `similar_artists`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `similar_artists` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `artist_id` int(10) unsigned NOT NULL,
  `similar_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `similar_artists_artist_id_similar_id_unique` (`artist_id`,`similar_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `similar_artists`
--

LOCK TABLES `similar_artists` WRITE;
/*!40000 ALTER TABLE `similar_artists` DISABLE KEYS */;
/*!40000 ALTER TABLE `similar_artists` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sitemap_ids`
--

DROP TABLE IF EXISTS `sitemap_ids`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sitemap_ids` (
  `id` int(10) unsigned NOT NULL,
  KEY `sitemap_ids_id_index` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sitemap_ids`
--

LOCK TABLES `sitemap_ids` WRITE;
/*!40000 ALTER TABLE `sitemap_ids` DISABLE KEYS */;
/*!40000 ALTER TABLE `sitemap_ids` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `social_profiles`
--

DROP TABLE IF EXISTS `social_profiles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `social_profiles` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_info_id` int(11) NOT NULL,
  `service_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_service_id` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `username` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `social_profiles_user_info_id_service_name_unique` (`user_info_id`,`service_name`),
  UNIQUE KEY `social_profiles_user_service_id_unique` (`user_service_id`),
  KEY `social_profiles_user_info_id_index` (`user_info_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `social_profiles`
--

LOCK TABLES `social_profiles` WRITE;
/*!40000 ALTER TABLE `social_profiles` DISABLE KEYS */;
/*!40000 ALTER TABLE `social_profiles` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `track_user_info`
--

DROP TABLE IF EXISTS `track_user_info`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `track_user_info` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `track_id` int(10) unsigned NOT NULL,
  `user_info_id` int(10) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `track_user_info_track_id_user_info_id_unique` (`track_id`,`user_info_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `track_user_info`
--

LOCK TABLES `track_user_info` WRITE;
/*!40000 ALTER TABLE `track_user_info` DISABLE KEYS */;
INSERT INTO `track_user_info` VALUES (1,3,18,'2018-03-22 14:00:21','2018-03-22 14:00:21');
/*!40000 ALTER TABLE `track_user_info` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tracks`
--

DROP TABLE IF EXISTS `tracks`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tracks` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `album_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `number` tinyint(3) unsigned NOT NULL,
  `duration` int(10) unsigned DEFAULT NULL,
  `artists` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `youtube_id` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `content_code` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `album_id` int(10) unsigned NOT NULL,
  `temp_id` varchar(8) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `url` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `plays` int(11) NOT NULL DEFAULT '0',
  `publisher_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `tracks_content_code_unique` (`content_code`),
  UNIQUE KEY `name_album_unique` (`name`(60),`album_name`(60)),
  KEY `tracks_number_index` (`number`),
  KEY `tracks_album_id_index` (`album_id`),
  KEY `tracks_artists` (`artists`(60)),
  KEY `tracks_temp_id_index` (`temp_id`),
  KEY `tracks_plays_index` (`plays`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tracks`
--

LOCK TABLES `tracks` WRITE;
/*!40000 ALTER TABLE `tracks` DISABLE KEYS */;
INSERT INTO `tracks` VALUES (3,'Be The One','Truth is a Beautiful thing',1,356938,'London Grammar',NULL,'',1,NULL,'http://78.157.209.138:9000/music/Djndp23c8bsPB9hswhfzPaFIF9TqYDbK03AuZxkx.mp3',1,18);
/*!40000 ALTER TABLE `tracks` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `uploads`
--

DROP TABLE IF EXISTS `uploads`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `uploads` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `file_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `file_size` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `mime` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `extension` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_info_id` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `url` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `thumbnail_url` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `uploads_file_name_unique` (`file_name`),
  KEY `uploads_name_index` (`name`),
  KEY `uploads_user_info_id_index` (`user_info_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `uploads`
--

LOCK TABLES `uploads` WRITE;
/*!40000 ALTER TABLE `uploads` DISABLE KEYS */;
/*!40000 ALTER TABLE `uploads` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user_info`
--

DROP TABLE IF EXISTS `user_info`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_info` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `reference` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `first_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `last_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `avatar` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `gender` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `permissions` text COLLATE utf8mb4_unicode_ci,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `language` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `country` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `timezone` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `user_info_reference_unique` (`reference`)
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_info`
--

LOCK TABLES `user_info` WRITE;
/*!40000 ALTER TABLE `user_info` DISABLE KEYS */;
INSERT INTO `user_info` VALUES (16,'admin','Administrator',NULL,NULL,NULL,'{\"admin\":1,\"superAdmin\":1}',NULL,'2018-03-20 16:10:33','2018-03-20 16:10:33',NULL,NULL,NULL),(18,'stikks','Franz','Kafka',NULL,NULL,'{\"artists.view\":1,\"albums.view\":1,\"tracks.view\":1,\"genres.view\":1,\"lyrics.view\":1,\"playlists.create\":1,\"playlists.view\":1,\"uploads.create\":1,\"artists.update\":1,\"albums.create\":1,\"albums.update\":1,\"albums.delete\":1,\"tracks.create\":1,\"tracks.update\":1,\"tracks.delete\":1}',NULL,'2018-03-21 10:55:02','2018-03-21 10:56:05','English','Nigeria','Africa/Lagos');
/*!40000 ALTER TABLE `user_info` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users_oauth`
--

DROP TABLE IF EXISTS `users_oauth`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users_oauth` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_info_id` int(11) NOT NULL,
  `service` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_oauth_user_info_id_service_unique` (`user_info_id`,`service`),
  UNIQUE KEY `users_oauth_token_unique` (`token`),
  KEY `users_oauth_user_info_id_index` (`user_info_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users_oauth`
--

LOCK TABLES `users_oauth` WRITE;
/*!40000 ALTER TABLE `users_oauth` DISABLE KEYS */;
/*!40000 ALTER TABLE `users_oauth` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-03-22 18:07:16
