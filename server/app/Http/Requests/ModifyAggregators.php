<?php
/**
 * Created by PhpStorm.
 * User: tm30
 * Date: 4/13/18
 * Time: 12:04 PM
 */

namespace App\Http\Requests;

use Illuminate\Validation\Rule;

class ModifyAggregators extends BaseFormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $aggregatorId = $this->route('id');

        $rules = [
            'name' => [
                'required', 'string', 'min:1', 'max:255',
                Rule::unique('aggregators')->ignore($aggregatorId)
            ],
            'address' => 'required|string',
            'bio' => 'required|string',
            'cac_number' => 'required|string',
        ];

//        if ($this->method() === 'POST') {
//            $rules = array_merge($rules, [
//                'albums' => 'array',
//                'albums.*.name' => 'required|string|min:1|max:255',
//                'albums.*.release_date' => 'string|min:1|max:255',
//                'albums.*.image' => 'string|min:1|max:255',
//                'albums.*.artist_id' => 'integer|exists:artists,id',
////                'albums.*.spotify_popularity' => 'integer|min:1|max:100',
//
//                'albums.*.tracks' => 'array',
//                'albums.*.tracks.*.name' => 'required|string|min:1|max:255',
//                'albums.*.tracks.*.album_name' => 'string|min:1|max:255',
//                'albums.*.tracks.*.number' => 'required|integer|min:1',
////                'albums.*.tracks.*.duration' => 'required|integer|min:1',
//                'albums.*.tracks.*.artists' => 'string|nullable',
//                'albums.*.tracks.*.youtube_id' => 'string|min:1|max:255',
////                'albums.*.tracks.*.spotify_popularity' => 'integer|min:1|max:100',
//                'albums.*.tracks.*.album_id' => 'integer|min:1',
//                'albums.*.tracks.*.url' => 'string|min:1|max:255',
//            ]);
//        }

        return $rules;
    }
}
