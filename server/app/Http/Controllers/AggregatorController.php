<?php
/**
 * Created by PhpStorm.
 * User: tm30
 * Date: 4/13/18
 * Time: 9:45 AM
 */

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Services\Aggregators\AggregatorsRepository;
use App\Aggregator;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use App\Http\Requests\ModifyAggregators;


class AggregatorController extends Controller
{

    /**
     * @var Request
     */
    private $request;

    /**
     * @var AggregatorsRepository
     */
    private $repository;

    /**
     * Create new ArtistController instance.
     *
     * @param Request $request
     * @param AggregatorsRepository $repository
     */
    public function __construct(Request $request, AggregatorsRepository $repository)
    {
        $this->request = $request;
        $this->repository = $repository;
    }

    /**
     * Paginate all artists.
     *
     * @return LengthAwarePaginator
     */
    public function index()
    {
        $this->authorize('index', Aggregator::class);
        return $this->repository->paginate($this->request->all());
    }

    /**
     * Return artist matching specified id or name.
     *
     * @param integer $nameOrId
     * @return array
     */
    public function show($nameOrId)
    {
        $this->authorize('show', Aggregator::class);

        if ($this->request->has('by_name')) {
            $data = $this->repository->getByName($nameOrId, $this->request->all());
        } else {
            $data = $this->repository->getById($nameOrId, $this->request->all());
        }

//        dispatch(new IncrementModelViews($data['artist']['id'], 'artist'));

        return $data;
    }

    /**
     * Create a new artist.
     *
     * @param ModifyAggregators $validate
     * @return Aggregator
     */
    public function store(ModifyAggregators $validate)
    {
        $this->authorize('store', Aggregator::class);

        if (!$this->repository->isPermissible($this->request->get('username'))) {
            return $this->error(['username'=>'An account with this username already exists'], 400);
        }

        return $this->repository->save($this->request->all());
    }

    /**
     * Update existing artist.
     *
     * @param  int $id
     * @param ModifyAggregators $validate
     * @return Aggregator
     */
    public function update($id, ModifyAggregators $validate)
    {
        $this->authorize('update', Aggregator::class);

        return $this->repository->update($id, $this->request->all());
    }

    /**
     * Remove specified artists from database.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy()
    {
        $this->authorize('destroy', Aggregator::class);

        $this->validate($this->request, [
            'ids'   => 'required|array',
            'ids.*' => 'required|integer'
        ]);

        $this->repository->delete($this->request->get('ids'));

        return $this->success();
    }

}