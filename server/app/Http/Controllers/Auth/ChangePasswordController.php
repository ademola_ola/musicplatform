<?php namespace App\Http\Controllers\Auth;

use Hash;
use App\UserInfo as User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\ApiRequest;

class ChangePasswordController extends Controller
{
    /**
     * @var Request
     */
    private $request;

    /**
     * @var User
     */
    private $user;

    /**
     * @var ApiRequest
     */
    private $apiRequest;

    /**
     * ChangePasswordController constructor.
     *
     * @param Request $request
     * @param User $user
     * @param ApiRequest $apiRequest
     */
    public function __construct(Request $request, User $user, ApiRequest $apiRequest)
    {
        $this->user = $user;
        $this->request = $request;
        $this->apiRequest = $apiRequest;
    }

    /**
     * Change specified user password.
     *
     * @param int $userId
     * @return User
     */
    public function change($userId)
    {
        $user = $this->user->findOrFail($userId);

//        $this->authorize('update', $user);

        $this->validate($this->request, $this->rules($user));

        $response = $this->apiRequest->postAuthorizedData('me/confirm', array('password' => $this->request->get('current_password')));

        if ($response['status'] == false) {
            return $this->error(['password' => $response['body']->message]);
        }

        $this->apiRequest->postAuthorizedData('me/reset', array('password' => $this->request->get('new_password')));

        if ($response['status'] == false) {
            return $this->error(['password' => $response['body']->message]);
        }

        return $user;
    }

    /**
     * Get validation rules for changing user password.
     *
     * @param User $user
     * @return array
     */
    private function rules(User $user)
    {
        $rules = [
            'new_password' => 'required|confirmed',
            'current_password' => 'required'
        ];

//        if ($user->hasPassword) {
//            $rules['current_password'] = "required|hash:{$user->password}";
//            $rules['new_password'] .= '|different:current_password';
//        }

        return $rules;
    }
}