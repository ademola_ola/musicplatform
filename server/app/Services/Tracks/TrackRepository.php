<?php

namespace App\Services\Tracks;

use App\Album;
use App\Artist;
use App\Services\Paginator;
use App\Services\Providers\ProviderResolver;
use App\Services\Settings;
use App\Track;
use function GuzzleHttp\Psr7\mimetype_from_extension;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;

class TrackRepository
{
    /**
     * @var Track
     */
    private $track;

    /**
     * AlbumRepository constructor.
     *
     * @param Track $track
     * @param Settings $settings
     * @param ProviderResolver $resolver
     */
    public function __construct(
        Track $track
    )
    {
        $this->track = $track;
    }

    /**
     * Paginate albums using specified parameters.
     *
     * @param array $params
     * @return LengthAwarePaginator
     */
    public function paginate($params = [])
    {
        return (new Paginator($this->track))->with('artist')->orderBy('number')->paginate($params);
    }

    /**
     * Create a new track.
     *
     * @param array $params
     * @return Track
     */
    public function create($params)
    {

        $apiRequest = app()->make('App\ApiRequest');
        $album = Album::whereId($params['album_id'])->first();
        $artist = Artist::whereId($album->artist_id)->first();

        $file = pathinfo($params['url']);

        $filePath = str_replace(config('app.url').'/', '', $params['url']);

        $data = [
            'name' => $params['name'],
            'owner' => $artist->user_info->reference,
            'publisher' => \Auth::user()->reference,
            'content_type_code' => 'music'
        ];

        $response = $apiRequest->postFile($filePath, mimetype_from_extension($file['extension']), $file['basename'], "contents", $data);

        if ($response['status'] == false) {
            return response()->json($response['body'], 422);
        }

        $params['publisher_id'] = \Auth::user()->id;
        $params['url'] = $response['body']->data;

        $track = $this->track->create($params);

        return $track;
    }

    /**
     * Delete specified tracks.
     *
     * @param array $ids
     * @return void
     */
    public function delete($ids)
    {
        $this->track->whereIn('id', $ids)->delete();
    }
}