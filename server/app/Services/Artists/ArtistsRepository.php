<?php namespace App\Services\Artists;

use App\Album;
use App\Artist;
use App\Genre;
use App\Services\Paginator;
use App\Track;
use App\UserInfo;
use Carbon\Carbon;
use App\Services\Settings;
use Illuminate\Support\Arr;
use Illuminate\Database\Eloquent\Collection;
use App\Services\Providers\ProviderResolver;
use App\Services\Utilities;

use DB;
use Illuminate\Pagination\LengthAwarePaginator;
use Cache;
use App\Aggregator;
use Auth;

class ArtistsRepository
{
    /**
     * @var Artist
     */
    private $artist;

    /**
     * @var Settings
     */
    private $settings;

    /**
     * @var ProviderResolver
     */
    private $resolver;

    /**
     * @var ArtistSaver
     */
    private $saver;

    /**
     * @var ArtistAlbumsPaginator
     */
    private $albumsPaginator;

    /**
     * @var ArtistBio
     */
    private $bio;

    /**
     * @var Album
     */
    private $album;

    /**
     * @var Track
     */
    private $track;

    /**
     * @var Genre
     */
    private $genre;

    /**
     * @var Aggregator
     */
    private $aggregator;

    /**
     * ArtistsRepository constructor.
     *
     * @param Artist $artist
     * @param Album $album
     * @param Track $track
     * @param Genre $genre
     * @param Settings $settings
     * @param ProviderResolver $resolver
     * @param ArtistSaver $saver
     * @param ArtistAlbumsPaginator $albumsPaginator
     * @param ArtistBio $bio
     * @param Aggregator $aggregator
     */
    public function __construct(
        Artist $artist,
        Album $album,
        Track $track,
        Genre $genre,
        Settings $settings,
        ProviderResolver $resolver,
        ArtistSaver $saver,
        ArtistAlbumsPaginator $albumsPaginator,
        ArtistBio $bio,
        Aggregator $aggregator
    )
    {
        $this->bio = $bio;
        $this->saver = $saver;
        $this->artist = $artist;
        $this->settings = $settings;
        $this->resolver = $resolver;
        $this->albumsPaginator = $albumsPaginator;
        $this->album = $album;
        $this->track = $track;
        $this->genre = $genre;
        $this->aggregator = $aggregator;
    }

    /**
     * Get artist by id.
     *
     * @param integer $id
     * @param array $params
     * @return array
     */
    public function getById($id, $params = [])
    {
        $artist = $this->artist->findOrFail($id);
        return $this->load($artist, $params);
    }

    /**
     * Get artist by name.
     *
     * @param string $name
     * @param array $params
     * @return array
     */
    public function getByName($name, $params = [])
    {
        $artist = $this->artist->where('name', $name)->first();

        if ( ! $artist && $this->settings->get('artist_provider') !== 'local') {
            $artist = $this->fetchAndStoreArtistFromExternal($name);
            if ( ! $artist) abort(404);
        }

        return $this->load($artist, $params);
    }

    /**
     * Load specified artist.
     *
     * @param Artist $artist
     * @param array $params
     * @return array|Artist
     */
    private function load(Artist $artist, $params = [])
    {
        //return only simplified version of specified artist if requested.
        if (Arr::get($params, 'simplified')) {
            return $artist->load('albums.tracks', 'genres');
        }

        $load = array_filter(explode(',', Arr::get($params, 'with', 'genres')));

        if ($this->needsUpdating($artist)) {
            $newArtist = $this->fetchAndStoreArtistFromExternal($artist->name);
            if ($newArtist) $artist = $newArtist;
        }

        $artist = $artist->load($load);

        $albums = $this->albumsPaginator->paginate($artist->id);

        $genres = DB::table('genre_artist')
            ->where('artist_id', '=', $artist->id)
            ->join('genres', 'genre_artist.genre_id', '=', 'genres.id')
            ->get();

        $response = $artist->toArray();

        $response['albums'] = $albums;

        $response['genres'] = $genres;

        if (Arr::get($params, 'top_tracks')) {
            $response['top_tracks'] = $this->getTopTracks($artist->name);
        }

        return $response;
    }

    /**
     * Fetch artist from external service and store it in database.
     *
     * @param string $name
     * @return Artist|null
     */
    private function fetchAndStoreArtistFromExternal($name)
    {
        $artist = null;
        $newArtist = $this->resolver->get('artist')->getArtist($name);

        if ($newArtist) {
            $artist = $this->saver->save($newArtist);
            $artist = $this->bio->get($artist);
            unset($artist['albums']);
        }

        return $artist;
    }

    /**
     * Get 20 most popular artists tracks.
     *
     * @param string $artistName
     * @return Collection
     */
    public function getTopTracks($artistName)
    {
        $tracks = Track::with('album.artist')
            ->where('artists', $artistName)
            ->orWhere('artists', 'like', $artistName.'*|*%')
            ->orderBy('plays', 'desc')
            ->limit(20)
            ->get();

        return $tracks;
    }

    /**
     * Paginate all artists using specified params.
     *
     * @param array $params
     * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator
     */
    public function paginate($params)
    {
        if ($params['aggregator_id'] == 0) {
            return (new Paginator($this->artist))->withCount('albums')->paginate($params);
        }

        $perPage = Arr::get($params, 'per_page', 15);
        $order_by = Arr::get($params, 'order_by', 'views');
        $orderDir = Arr::get($params, 'order_dir', 'desc');
        $page = Arr::get($params, 'page', 1);

        $query = DB::table('aggregated_artists')
            ->select(array('artists.*', DB::raw('COUNT(albums.artist_id) as album_count')))
            ->where('aggregator_id', '=', $params['aggregator_id'])
            ->join('artists', 'aggregated_artists.artist_id', '=', 'artists.id')
            ->leftJoin('albums', 'artists.id', '=', 'albums.artist_id')
            ->groupBy('artists.id')
            ->orderBy($order_by, $orderDir);

        $key = "pagination.{$this->artist->getTable()}_count";

        $count = Cache::remember($key, Carbon::now()->addDay(), function () {
            return $this->artist->count();
        });

        return new LengthAwarePaginator(
            $query->skip(($page - 1) * $perPage)->take($perPage)->get(),
            $count,
            $perPage,
            $page
        );
    }

    /**
     * Create remote provider
     *
     * @param array $params
     * @param boolean $noUser
     * @return Artist
     */
    public function create($noUser, $params)
    {
        $albums = Arr::pull($params, 'albums', []);
        $genres = Arr::pull($params, 'genres', []);

        $data = ['username' => $params['username']];

        if ($noUser) {
            $data['password'] = Utilities::generateRandomString(10);
            $name = explode(' ', $params['name']);
            $data['first_name'] = $name[0];
            if (count($name) > 1) {
                unset($name[0]);
                $data['last_name'] = join(' ', $name);
            }
        }

        if (isset($params['aggregator_id'])) {
            $data['manager_username'] = Auth::user()->reference;
        }

        $apiRequest = app()->make('App\ApiRequest');
        $response = $apiRequest->basicPost("providers", $data);

        if ($response['status'] == false) {
            return response()->json($response['body'], 422);
        }

        $userRepository = app()->make('App\Services\Auth\UserInfoRepository');

        if ($noUser) {
            $data['reference'] = $data['username'];
            $data['permissions'] = config('permissions.artist');
            $userRepository->create($data);
        }

        $user = UserInfo::where('reference', $params['username'])->first();
        $params['user_info_id'] = $user->id;
        $artist = $this->artist->create(Arr::except($params, ['created_at', 'updated_at', 'aggregator_id']));
        $user->setPermissionsAttribute(config('permissions.artist'));
        $user->save();

        foreach ($albums as $album) {
            $tracks = Arr::pull($album, 'tracks', []);

            //create album
            $album['artist_id'] = $artist->id;
            $album['fully_scraped'] = 1;
            $album = $this->album->create($album);

            //set album name, id and artist name on each track
            $tracks = array_map(function($track) use($album, $artist) {
//                $track['spotify_popularity'] = Arr::get($track, 'spotify_popularity', 50);
                $track['url'] = Arr::get($track, 'url', null);
                $track['youtube_id'] = Arr::get($track, 'youtube_id', null);
                $track['album_name'] = $album->name;
                $track['album_id'] = $album->id;
                return $track;
            }, $tracks);

            $this->track->insert($tracks);
        }

        //attach genres
        $genreIds = collect($genres)->map(function($genre) {
            return $this->genre->firstOrCreate(['name' => $genre['name']]);
        })->pluck('id');

        $artist->genres()->attach($genreIds);

        if (isset($params['aggregator_id'])) {
            DB::table('aggregated_artists')->insertGetId([
                'artist_id' => $artist->id,
                'aggregator_id' => $params['aggregator_id']
            ]);
        }

        return $artist->load('albums.tracks', 'genres');
    }

    /**
     * Update existing artist.
     *
     * @param integer $id
     * @param array $params
     * @return Artist
     */
    public function update($id, $params)
    {
        $artist = $this->artist->findOrFail($id);

        $ids = collect(Arr::pull($params, 'genres', []))->map(function($genre) {
            return $this->genre->firstOrCreate(['name' => $genre['name']]);
        })->pluck('id');

        $artist->genres()->sync($ids);

        $artist->fill(Arr::except($params, ['albums', 'genres', 'created_at', 'updated_at', 'aggregator_id']))->save();

        return $artist->load('albums.tracks', 'genres');
    }

    /**
     * Delete specified artists from database.
     *
     * @param array $ids
     */
    public function delete($ids)
    {
        $albumIds = $this->album->whereIn('artist_id', $ids)->pluck('id');

        $this->artist->whereIn('id', $ids)->delete();
        $this->album->whereIn('id', $albumIds)->delete();
        $this->track->whereIn('album_id', $albumIds)->delete();
    }

    /**
     * Check if specified artist needs to be updated via external site.
     *
     * @param Artist $artist
     * @return bool
     */
    private function needsUpdating(Artist $artist)
    {
        if ($this->settings->get('artist_provider', 'local') === 'local') return false;

        if ( ! $artist->fully_scraped) return true;

        $updateInterval = $this->settings->get('automation.artist_interval', 7);

        //0 means that artist should never be updated from 3rd party sites
        if ($updateInterval === 0) return false;

        return $artist->updated_at->addDays($updateInterval) <= Carbon::now();
    }

    /*
     *  check if artist record can be created for user with specified username
     * @param $username
     * @return bool
     */
    public function isPermissible($username) {
        $user = UserInfo::where('reference', $username)->first();
        if ($user) {
            return !$this->artist->where('user_info_id', $user->id)->exists()
                && !$user->hasPermission('admin');
        }
        return true;
    }

    /*
     * create artist and user account
     */
    public function save(Array $data) {
        return $this->create(UserInfo::where('reference', $data['username'])->exists() == null, $data);
    }
}