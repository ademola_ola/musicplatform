<?php
/**
 * Created by PhpStorm.
 * User: tm30user
 * Date: 02/03/2018
 * Time: 1:33 PM
 */

namespace App\Services;

use App\UserInfo;
use Mookofe\Tail\Facades\Tail;
use Illuminate\Support\Facades\Log;

class Utilities
{
    static public function generateRandomString($len) {
        return substr(str_shuffle(str_repeat($character='0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ', ceil($len/strlen($character)) )),1,$len);
    }

    static public function broadcastAction($queue, $payload=array('*')) {
        try {
            Tail::add($queue, json_encode($payload), array('content_type' => 'application/json'));
        }  catch (\Exception $e) {
            Log::error($e);
        }
    }

    static public function createApiUser($data=array('*')) {
        $params = array(
            'permissions' => config('permissions.user'),
            'reference' => $data['username'],
            'first_name' => $data['first_name'],
            'last_name' => $data['last_name']
        );
        foreach ($data['roles'] as $role) {
            if ($role->slug == 'administrator') {
                $params['permissions'] = ['admin' => 1, 'superAdmin' => 1];
            }
        }

        return UserInfo::create($params);
    }

}