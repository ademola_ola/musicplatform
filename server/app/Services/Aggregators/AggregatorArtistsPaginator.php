<?php
/**
 * Created by PhpStorm.
 * User: tm30
 * Date: 4/13/18
 * Time: 11:11 AM
 */

namespace App\Services\Aggregators;
use App\Artist;
use DB;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;


class AggregatorArtistsPaginator
{
    /**
     * @var Artist
     */
    private $artist;

    /**
     * ArtistRepository constructor.
     *
     * @param Artist $artist
     */
    public function __construct(Artist $artist)
    {
        $this->artist = $artist;
    }

    /**
     * Paginate all specified aggregator's artists.
     *
     * First order by number of tracks, so all artists
     * with less then 5 tracks (singles) are at
     * the bottom, then order by album release date.
     *
     * @param integer $aggregatorId
     * @return LengthAwarePaginator
     */
    public function paginate($aggregatorId)
    {
//        $prefix = DB::getTablePrefix();

        return DB::table('aggregated_artists')
            ->select('*')
            ->where('aggregator_id', $aggregatorId)
            ->join('artists', 'artists.id', '=', 'aggregated_artists.artist_id')
            ->paginate(5);
//        return $this->artist
////            ->with('albums')
//            ->join('aggregated_artists', 'aggregated_artists.aggregator_id', '=', 'aggregators.id')
//            ->where('aggregator_id', $aggregatorId)
//            ->selectRaw("{$prefix}artists.*")
////            ->leftjoin('albums', 'albums.artist_id', '=', 'artists.id')
//            ->groupBy('artists.id')
//            ->orderByRaw("COUNT({$prefix}albums.id) > 5 desc, {$prefix}artists.created_at desc")
//            ->paginate(5);
    }
}