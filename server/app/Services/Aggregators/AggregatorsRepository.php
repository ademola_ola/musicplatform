<?php
/**
 * Created by PhpStorm.
 * User: tm30
 * Date: 4/13/18
 * Time: 9:48 AM
 */

namespace App\Services\Aggregators;

use App\Aggregator;
use App\Services\Providers\ProviderResolver;
use App\Artist;
use Illuminate\Support\Arr;
use App\Services\Aggregators\AggregatorArtistsPaginator;
use App\Services\Settings;
use App\Services\Paginator;
use App\UserInfo;
use App\Services\Utilities;

class AggregatorsRepository
{
    /**
     * @var Aggregator
     */
    private $aggregator;

    /**
     * @var Settings
     */
    private $settings;

    /**
     * @var ProviderResolver
     */
    private $resolver;

    /**
     * @var AggregatorArtistsPaginator
     */
    private $artistsPaginator;

    /**
     * @var Artist
     */
    private $artist;

    /**
     * ArtistsRepository constructor.
     *
     * @param Artist $artist
     * @param Aggregator $aggregator
     * @param Settings $settings
     * @param ProviderResolver $resolver
     * @param AggregatorArtistsPaginator $artistsPaginator
     */
    public function __construct(
        Artist $artist,
        Aggregator $aggregator,
        Settings $settings,
        ProviderResolver $resolver,
        AggregatorArtistsPaginator $artistsPaginator
    )
    {
        $this->aggregator = $aggregator;
        $this->artist = $artist;
        $this->settings = $settings;
        $this->resolver = $resolver;
        $this->artistsPaginator = $artistsPaginator;
    }

    /**
     * Get aggregator by id.
     *
     * @param integer $id
     * @param array $params
     * @return array
     */
    public function getById($id, $params = [])
    {
        $aggregator = $this->aggregator->findOrFail($id);
        return $this->load($aggregator, $params);
    }

    /**
     * Get aggregator by name.
     *
     * @param string $name
     * @param array $params
     * @return array
     */
    public function getByName($name, $params = [])
    {
        $aggregator = $this->aggregator->where('name', $name)->first();

        return $this->load($aggregator, $params);
    }

    /**
     * Load specified aggregator.
     *
     * @param Aggregator $aggregator
     * @param array $params
     * @return array|Artist
     */
    private function load(Aggregator $aggregator, $params = [])
    {

        $load = array_filter(explode(',', Arr::get($params, 'with', '')));

        $aggregator = $aggregator->load($load);

        $artists = $this->artistsPaginator->paginate($aggregator->id);

        $response = ['aggregator' => $aggregator, 'artists' => $artists];

        return $response;
    }

    /**
     * Paginate all aggregators using specified params.
     *
     * @param array $params
     * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator
     */
    public function paginate($params)
    {
        return (new Paginator($this->aggregator))->withCount('artists')->paginate($params);
    }

    /**
     * Create remote provider
     *
     * @param array $params
     * @param boolean $noUser
     * @return Aggregator
     */
    public function create($noUser, $params)
    {
        $data = ['username' => $params['username']];

        if ($noUser) {
            $data['password'] = Utilities::generateRandomString(10);
            $name = explode(' ', $params['name']);
            $data['first_name'] = $name[0];
            if (count($name) > 1) {
                unset($name[0]);
                $data['last_name'] = join(' ', $name);
            }
        }

        $apiRequest = app()->make('App\ApiRequest');
        $response = $apiRequest->basicPost("managers", $data);

        if ($response['status'] == false) {
            $errorData = ['status' => 'error', 'messages' => []];
            $errorData['messages']['username'] = $response['body'];
            return response()->json($errorData, 400);
        }

        $userRepository = app()->make('App\Services\Auth\UserInfoRepository');

        $permissions = config('permissions.aggregator');

        if ($noUser) {
            $data['reference'] = $data['username'];
            $data['permissions'] = $permissions;
            $userRepository->create($data);
        }

        $user = UserInfo::where('reference', $params['username'])->first();
        $params['user_info_id'] = $user->id;
        unset($params['artists']);
        $aggregator = $this->aggregator->create($params);
        $user->setPermissionsAttribute($permissions);
        $user->save();

        return $aggregator->load('artists');
    }

    /**
     * Update existing artist.
     *
     * @param integer $id
     * @param array $params
     * @return Artist
     */
    public function update($id, $params)
    {
        $aggregator = $this->aggregator->findOrFail($id);

        $aggregator->fill(Arr::except($params, ['username', 'created_at', 'updated_at']))->save();

        return $aggregator->load('artists');
    }

    /**
     * Delete specified artists from database.
     *
     * @param array $ids
     */
    public function delete($ids)
    {

        $this->aggregator->whereIn('id', $ids)->delete();
    }

    /*
     *  check if artist record can be created for user with specified username
     * @param $username
     * @return bool
     */
    public function isPermissible($username) {
        $user = UserInfo::where('reference', $username)->first();
        if ($user) {
            return !$this->artist->where('user_info_id', $user->id)->exists() &&
                !$this->aggregator->where('user_info_id', $user->id)->exists() && !$user->hasPermission('admin');
        }
        return true;
    }

    /*
     * create artist and user account
     */
    public function save(Array $data) {
        return $this->create(UserInfo::where('reference', $data['username'])->exists() == null, $data);
    }

}