<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Artist;
use App\Aggregator;
use App\Services\Auth\UserInfoRepository;
use App\UserInfo;

class PermissionsFix extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'permissions:fix';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Fix permissions for artists and aggregators';

    /**
     * @var Artist
     */
    private $artist;

    /**
     * @var Aggregator
     */
    private $aggregator;

    /**
     * @var UserInfoRepository
     */
    private $userInfoRepository;

    /**
     * @var UserInfo
     */
    private $user;

    /**
     * Create a new command instance.
     *
     * @param $artist
     * @param $aggregator
     * @param $userInfoRepository
     * @param $user
     * @return void
     */
    public function __construct(Artist $artist, Aggregator $aggregator, UserInfoRepository $userInfoRepository, UserInfo $user)
    {
        parent::__construct();

        $this->artist = $artist;
        $this->aggregator = $aggregator;
        $this->userInfoRepository = $userInfoRepository;
        $this->user = $user;
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */

    public function handle() {

        foreach ($this->user->all() as $use) {
            $use->forceFill(['permissions' => config('permissions.user')])->save();
        }

        foreach ($this->artist->all() as $artist) {
            $user = $this->userInfoRepository->findOrFail($artist->user_info_id);
            $user->forceFill(['permissions' => config('permissions.artist')])->save();

        }

        foreach ($this->aggregator->all() as $agg) {
            $agg_user = $this->userInfoRepository->findOrFail($agg->user_info_id);
            $agg_user->forceFill(['permissions' => config('permissions.aggregator')])->save();
        }

        $admin = $this->user->where('reference', 'admin')->first();
        $admin->forceFill(['permissions' => ['admin' => 1, 'superAdmin' => 1]])->save();

        return true;
    }
}
