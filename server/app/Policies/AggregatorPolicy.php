<?php
/**
 * Created by PhpStorm.
 * User: tm30
 * Date: 4/13/18
 * Time: 11:29 AM
 */

namespace App\Policies;

use Illuminate\Auth\Access\HandlesAuthorization;
use App\UserInfo;

class AggregatorPolicy
{
    use HandlesAuthorization;

    public function index(UserInfo $aggregator)
    {
        return $aggregator->hasPermission('aggregators.view');
    }

    public function show(UserInfo $aggregator)
    {
        return $aggregator->hasPermission('aggregators.view');
    }

    public function store(UserInfo $aggregator)
    {
        return $aggregator->hasPermission('aggregators.create');
    }

    public function update(UserInfo $aggregator)
    {
        return $aggregator->hasPermission('aggregators.update');
    }

    public function destroy(UserInfo $aggregator)
    {
        return $aggregator->hasPermission('aggregators.delete');
    }
}