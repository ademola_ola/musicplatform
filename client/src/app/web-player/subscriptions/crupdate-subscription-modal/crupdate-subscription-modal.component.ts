///<reference path="../../../../../node_modules/@angular/core/src/metadata/directives.d.ts"/>
import {Component, ElementRef, Renderer2, ViewEncapsulation} from '@angular/core';
import {BaseModalClass} from "../../../shared/modal/base-modal";
import {Observable} from "rxjs/Observable";
import {User} from "../../../shared/types/models/User";
import {Users} from "../../users/users.service";
import {CurrentUser} from "../../../auth/current-user";

@Component({
    selector: 'crupdate-subscription-modal',
    templateUrl: './crupdate-subscription-modal.component.html',
    styleUrls: ['./crupdate-subscription-modal.component.scss'],
    encapsulation: ViewEncapsulation.None
})

export class CrupdateSubscriptionModalComponent extends BaseModalClass {

    /**
     * NewSubscriptionModal Component.
     */
    constructor(
        protected el: ElementRef,
        protected renderer: Renderer2,
        public user: CurrentUser,
        private users: Users,
        // private model: Object
    ) {
        super(el, renderer);
    }

    /**
     * Show the modal.
     */
    public show(params: {user?: CurrentUser}) {
        super.show(params);
    }

    /**
     * Close modal and emit crupdated playlist.
     */
    public confirm() {
        this.loading = true;

        this.subscribeUser().finally(() => {
            this.loading = false;
        }).subscribe(response => {
            super.done(response);
        }, this.handleErrors.bind(this));
    }

    private subscribeUser(): Observable<Object> {

        return this.users.subscribe(this.user.current);
    }
}
