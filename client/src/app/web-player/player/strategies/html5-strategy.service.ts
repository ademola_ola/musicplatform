import {PlaybackStrategy} from "./playback-strategy.interface";
import {PlayerState} from "../player-state.service";
import {Track} from "../../../shared/types/models/Track";
import {Injectable, NgZone} from "@angular/core";
import {PlayerQueue} from "../player-queue.service";
import {Howl, Howler} from "howler";
import {CurrentUser} from "../../../auth/current-user";
// import {Howl} from "howler";

@Injectable()
export class Html5Strategy implements PlaybackStrategy {

    /**
     * Whether player is already bootstrapped.
     */
    private bootstrapped = false;

    /**
     * Volume that should be set after player is bootstrapped.
     * Number between 1 and 100.
     */
    private pendingVolume: number = null;

    /**
     * Html5 video element instance.
     */
    private html5: HTMLVideoElement;

    /**
     * Track currently cued for playback.
     */
    private cuedTrack: Track;

    /**
     * Track that is currently being cued.
     */
    private cueing: Track;

    private sound;

    /**
     * Html5Strategy Constructor.
     */
    constructor(
        private state: PlayerState,
        private zone: NgZone,
        private queue: PlayerQueue,
        private user: CurrentUser

    ) {
        // this.sound = new Howl({
        //     src: [],
        //     html5: true
        // });
    }

    /**
     * Start playback.
     */
    public async play() {
        let current_track = this.queue.getCurrent();
        await this.cueTrack(current_track);
        this.html5.play();

        // // check if user is logged on
        // if (this.user.isLoggedIn()) {
        //     this.html5.play();
        // }
        // else {
        //
        //     this.sound = new Howl({
        //         src: [current_track.url],
        //         sprite: {
        //             spliced: [start, end]
        //         },
        //         html5: true,
        //         onpause: function () {
        //             console.log('song paused')
        //         },
        //         onseek: function () {
        //             console.log('song seeked')
        //         },
        //         onstop: function () {
        //             console.log('song stopped')
        //         },
        //         onend: function() {
        //             console.log('song ended')
        //         }
        //     });
        //     this.sound.play('spliced');
        //     // this.sound.fade(1,0, 30000, this.sound.play());
        // }
        this.state.playing = true;
    }

    /**
     * Pause playback.
     */
    public pause() {
        // if (this.user.isLoggedIn()) {
        //     this.html5.pause();
        // }
        // else {
        //     this.sound.pause();
        // }
        this.html5.pause();
        this.state.playing = false;
    }

    /**
     * Stop playback.
     */
    public stop() {
        // if (this.user.isLoggedIn()) {
        //     this.html5.pause();
        //     this.seekTo(0);
        // }
        // else {
        //     this.sound.pause();
        //     this.sound.seek(0);
        // }

        this.html5.pause();
        this.seekTo(0);
        this.state.playing = false;
    }

    /**
     * Seek to specified time in track.
     */
    public seekTo(time: number) {
        this.html5.currentTime = time;
        // if (this.user.isLoggedIn()) {
        //     this.html5.currentTime = time;
        // }
        // else {
        //     this.sound.seek(time);
        // }
    }

    /**
     * Get loaded track duration in seconds.
     */
    public getDuration() {
        if ( ! this.html5.seekable.length) return 0;
        return this.html5.seekable.end(0);
        // if (this.user.isLoggedIn()) {
        //     if ( ! this.html5.seekable.length) return 0;
        //     return this.html5.seekable.end(0);
        // }
        // else {
        //     this.sound.duration();
        // }
    }

    /**
     * Get elapsed time in seconds since the track started playing
     */
    public getCurrentTime() {
        // return this.user.isLoggedIn() ? this.html5.currentTime : this.sound.seek();
        return this.html5.currentTime;
    }

    /**
     * Set html5 player volume to float between 0 and 1.
     */
    public setVolume(number: number) {
       // if (this.user.isLoggedIn()) {
       //     if ( ! this.html5) {
       //         this.pendingVolume = number;
       //     } else {
       //         this.html5.volume = number / 100;
       //     }
       // }
       // else {
       //     this.sound !== undefined ? this.sound.volume(number/100): Howler.volume(number/100);
       // }
        if ( ! this.html5) {
            this.pendingVolume = number;
        } else {
            this.html5.volume = number / 100;
        }
    }

    /**
     * Mute the player.
     */
    public mute() {
        this.html5.muted = true
        // this.user.isLoggedIn() ? this.html5.muted = true : this.sound.mute(true);
    }

    /**
     * Unmute the player.
     */
    public unMute() {
        this.html5.muted = false
        // this.user.isLoggedIn() ? this.html5.muted = false : this.sound.mute(false);
    }

    /**
     * Get track that is currently cued for playback.
     */
    public getCuedTrack(): Track {
        return this.cuedTrack;
    }

    /**
     * Check if youtube player is ready.
     */
    public ready() {
        return this.bootstrapped;
    }

    /**
     * Fetch youtube ID for specified track if needed and cue it in youtube player.
     */
    public async cueTrack(track: Track): Promise<any> {
        if (this.cueing === track || this.cuedTrack === track) return;

        this.cueing = track;

        this.state.buffering = true;

        this.bootstrap();
        this.html5.src = track.url;

        if (!this.user.isLoggedIn() || !this.user.isSubscribed() && !this.user.isAdmin()) {
            let start = 0;
            let duration = track.duration / 1000;
            let end = duration;

            if (duration > 30) {
                end = duration;
                start = end - 30;
            }

            this.html5.src += '#t=' + start + ',' + end;
        }

        this.cuedTrack = track;
        this.cueing = null;
        return new Promise(resolve => resolve());
    }

    /**
     * Destroy html5 playback strategy.
     */
    public destroy() {
        this.html5 && this.html5.remove();
        this.html5 = null;
        this.bootstrapped = false;
        this.cuedTrack = null;
    }

    /**
     * Bootstrap html5 player.
     */
    private bootstrap() {
        if (this.bootstrapped) return;

        this.html5 = document.createElement('video');
        this.html5.setAttribute('playsinline', 'true');
        this.html5.id = 'html5-player';
        document.querySelector('.html5-player').appendChild(this.html5);

        this.handlePlayerReadyEvent();
        this.handlePlayerStateChangeEvents();

        this.bootstrapped = true;
    }

    /**
     * Handle html5 playback state change events.
     */
    private handlePlayerStateChangeEvents() {
        this.html5.addEventListener('ended', () => {
            this.state.firePlaybackEnded();
            this.setState('playing', false);
        });

        this.html5.addEventListener('playing', () => {
            this.setState('playing', true);
        });

        this.html5.addEventListener('pause', () => {
            this.setState('playing', false);
        });

        this.html5.addEventListener('error', () => {
            this.cuedTrack = null;
            this.setState('playing', false);
            this.state.firePlaybackEnded();
        });
    }

    /**
     * Set specified player state.
     */
    private setState(name: string, value: boolean) {
        this.zone.run(() => this.state[name] = value);
    }

    /**
     * Handle html5 player ready event.
     */
    private handlePlayerReadyEvent(resolve?) {
        if (this.state.muted) this.mute();
        this.bootstrapped = true;
        resolve && resolve();
        this.state.fireReadyEvent();

        if (this.pendingVolume) {
            this.setVolume(this.pendingVolume);
            this.pendingVolume = null;
        }
    }
}