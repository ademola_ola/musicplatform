import {Component, OnInit, ViewEncapsulation} from '@angular/core';
import {Settings} from "../../../shared/settings.service";
import {ModalService} from "../../../shared/modal/modal.service";
import {ActivatedRoute, Router} from "@angular/router";
import {ToastService} from "../../../shared/toast/toast.service";
import {Aggregators} from "../aggregators.service";
import {Aggregator} from "../../../shared/types/models/Aggregator";
import {CurrentUser} from "../../../auth/current-user";

@Component({
    selector: 'new-aggregator-page',
    templateUrl: './new-aggregator-page.component.html',
    styleUrls: ['./new-aggregator-page.component.scss'],
    encapsulation: ViewEncapsulation.None
})
export class NewAggregatorPageComponent implements OnInit {

    /**
     * Backend validation errors from last request.
     */
    public errors: any = {};

    /**
     * New artist model.
     */
    public aggregator = new Aggregator({artists: [], bio: '', address: '', cac_number: ''});

    /*
     * loading
     */
    public loading: boolean;

    /**
     * NewArtistPageComponent Constructor.
     */
    constructor(
        private settings: Settings,
        private modal: ModalService,
        private aggregators: Aggregators,
        private route: ActivatedRoute,
        private toast: ToastService,
        private router: Router,
        private user: CurrentUser
    ) {}

    ngOnInit() {
        this.bindToRouteData();
    }

    /**
     * Create or update aggregator based on model id.
     */
    public createOrUpdate() {
        this.aggregator.id ? this.update() : this.create();
    }

    /**
     * Create a new aggregator.
     */
    public create() {
        this.loading = true;
        return this.aggregators.create(this.getPayload()).subscribe(aggregator => {
            this.loading = false;
            this.aggregator = aggregator;
            this.toast.show('Aggregator created.');
            this.errors = {};
            this.router.navigate(['/admin/aggregators', this.aggregator.id, 'edit'], {replaceUrl: true})
        }, errors => {
            this.loading = false;
            console.log(errors);
            this.errors = errors.messages;
        });
    }

    /**
     * Update existing artist.
     */
    public update() {
        return this.aggregators.update(this.aggregator.id, this.getPayload()).subscribe(aggregator => {
            this.aggregator = aggregator;
            this.toast.show('Aggregator updated.');
            this.errors = {};
        }, errors => {
            this.errors = errors.messages;
        });
    }

    /**
     * Get payload for creating new artist or updating existing one.
     */
    private getPayload() {
        return Object.assign({}, this.aggregator);
    }

    /**
     * Bind to route data and hydrate artist model.
     */
    private bindToRouteData() {
        this.route.data.subscribe(response => {
            if (response.data) {
                this.aggregator = response.data.aggregator;
                this.aggregator.artists = response.data.artists.data;
                console.log(this.aggregator.artists);
            }
        });
    }
}
