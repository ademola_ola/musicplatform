import {Component, Input, OnInit, ViewEncapsulation} from '@angular/core';
import {FormControl} from "@angular/forms";
import {ModalService} from "../../../../shared/modal/modal.service";
import {Artist} from "../../../../shared/types/models/Artist";
import {ConfirmModalComponent} from "../../../../shared/modal/confirm-modal/confirm-modal.component";
import {Settings} from "../../../../shared/settings.service";
import {utils} from "../../../../shared/utils";

import {Aggregator} from "../../../../shared/types/models/Aggregator";
import {Artists} from "../../../../web-player/artists/artists.service";

@Component({
    selector: 'aggregator-artists-table',
    templateUrl: './aggregator-artists-table.component.html',
    styleUrls: ['./aggregator-artists-table.component.scss'],
    encapsulation: ViewEncapsulation.None
})
export class AggregatorArtistsTableComponent implements OnInit {

    /**
     * Form control for DataTable search input.
     */
    public searchQuery = new FormControl();

    /**
     * Whether all tracks are currently selected.
     */
    public allArtistsSelected: boolean = false;

    /**
     * All currently selected albums.
     */
    public selectedArtists = [];

    /**
     * Albums filtered by search query.
     */
    public filteredArtists: Artist[] = [];

    /**
     * Artist albums belong to.
     */
    @Input() aggregator: Aggregator;

    /**
     * ArtistAlbumsTableComponent Constructor.
     */
    constructor(
        private modal: ModalService,
        private artists: Artists,
        private settings: Settings
    ) {}

    ngOnInit() {
        this.bindSearchQuery();
        this.filteredArtists = this.aggregator.artists.slice();
    }

    /**
     * Confirm and delete selected artists.
     */
    public maybeDeleteSelectedArtists() {
        this.modal.show(ConfirmModalComponent, {
            title: 'Delete Artists',
            body: 'Are you sure you want to delete selected artists?',
            ok: 'Delete'
        }).onDone.subscribe(async () => {
            let ids = this.selectedArtists.filter(identifier => {
                return this.filteredArtists.find(curr => this.getIdentifier(curr) === identifier).id;
            });

            if (ids.length) {
                await this.artists.delete(ids).toPromise();
            }

            this.selectedArtists.forEach(identifier => {
                let i = this.filteredArtists.findIndex(curr => this.getIdentifier(curr) === identifier);
                let k = this.aggregator.artists.findIndex(curr => this.getIdentifier(curr) === identifier);
                this.filteredArtists.splice(i, 1);
                this.aggregator.artists.splice(k, 1);
            });

            this.deselectAllItems();
        });
    }

    /**
     * Open modal for creating a new album.
     */
    // public openNewAlbumModal() {
    //     this.deselectAllItems();
    //
    //     this.modal.show(CrupdateAlbumModalComponent, {artist: this.artist}).onDone.subscribe(album => {
    //         album = this.setIdentifier(album);
    //         this.artist.albums.push(album);
    //         this.filteredAlbums.push(album);
    //     });
    // }

    /**
     * Open modal for editing existing album.
     */
    // public openEditAlbumModal(album: Album) {
    //     this.deselectAllItems();
    //
    //     this.modal.show(CrupdateAlbumModalComponent, {artist: this.artist, album}).onDone.subscribe(album => {
    //         let i = this.filteredAlbums.findIndex(curr => this.getIdentifier(curr) === this.getIdentifier(album));
    //         let k = this.artist.albums.findIndex(curr => this.getIdentifier(curr) === this.getIdentifier(album));
    //         album = this.setIdentifier(album);
    //         this.filteredAlbums[i] = album;
    //         this.artist.albums[k] = album;
    //     });
    // }

    /**
     * Check if given album is currently selected.
     */
    public isArtistSelected(album: string) {
        return this.selectedArtists.indexOf(album) > -1;
    }

    /**
     * Selected or deselect specified album.
     */
    public toggleSelectedArtist(album: string) {
        let index = this.selectedArtists.indexOf(album);

        if (index > -1) {
            this.selectedArtists.splice(index, 1);
        } else {
            this.selectedArtists.push(album);
        }
    }

    /**
     * Select or de-select all tracks.
     */
    public toggleAllSelectedArtists() {
        if (this.allArtistsSelected) {
            this.selectedArtists = [];
        } else {
            this.selectedArtists = this.filteredArtists.map(album => this.getIdentifier(album));
        }

        this.allArtistsSelected = !this.allArtistsSelected;
    }

    /**
     * Deselect all currently selected items.
     */
    public deselectAllItems() {
        this.selectedArtists = [];
        this.allArtistsSelected = false;
    }

    // /**
    //  * Get available album image url or default one.
    //  */
    // public getAlbumImage(album: Album): string {
    //     if (album.image) return album.image;
    //     return this.settings.getBaseUrl() + 'assets/images/default/album.png';
    // }

    /**
     * If album is not created in backend yet, assign an identifier
     * by which album can by found in albums array for editing.
     */
    public setIdentifier(artist: Artist): Artist {
        return artist;
    }

    /**
     * Get artist identifier.
     */
    public getIdentifier(artist: Artist) {
        return artist.id;
    }

    /**
     * Bind artist list search form control.
     */
    private bindSearchQuery() {
        this.searchQuery.valueChanges
            .debounceTime(150)
            .distinctUntilChanged()
            .subscribe(query => {
                let artists = this.aggregator.artists.slice();
                this.filteredArtists = artists.filter(artist => {
                    return utils.strContains(artist.name, query);
                });
            });
    }
}
