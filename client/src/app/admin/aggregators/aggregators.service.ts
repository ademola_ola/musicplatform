import {Injectable} from '@angular/core';
import {AppHttpClient} from "../../shared/app-http-client.service";
import {Observable} from "rxjs/Observable";
import {Aggregator} from "../../shared/types/models/Aggregator";
import {PaginationResponse} from "../../shared/types/pagination-response";
import {Artist} from "../../shared/types/models/Artist";

@Injectable()
export class Aggregators {

    /**
     * Artists Service Constructor.
     */
    constructor(private httpClient: AppHttpClient) {}

    /**
     * Get artist matching specified id.
     */
    public get(id: number, params = {}): Observable<{aggregator: Aggregator, artists: PaginationResponse<Artist>}> {
        return this.httpClient.get('aggregators/' + id, params);
    }

    /**
     * Create a new artist.
     */
    public create(payload: object): Observable<Aggregator> {
        return this.httpClient.post('aggregators', payload);
    }

    /**
     * Update existing artist.
     */
    public update(id: number, payload: object): Observable<Aggregator> {
        return this.httpClient.put('aggregators/'+id, payload);
    }

    /**
     * Paginate specified artist albums.
     */
    public paginateAggregatorArtists(id: number, page = 1): Observable<PaginationResponse<Artist>> {
        return this.httpClient.get('aggregators/'+id+'/artists', {page});
    }

    // /**
    //  * Get radio recommendations for specified artist.
    //  */
    // public getRadioRecommendations(id: number) {
    //     return this.httpClient.get(`radio/artist/${id}`);
    // }

    /**
     * Delete specified artists.
     */
    public delete(ids: number[]) {
        return this.httpClient.delete('aggregators', {ids});
    }
}
