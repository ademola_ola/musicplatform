import {Component, OnDestroy, OnInit, ViewEncapsulation} from '@angular/core';
import {ModalService} from "../../shared/modal/modal.service";
import {ConfirmModalComponent} from "../../shared/modal/confirm-modal/confirm-modal.component";
import {CurrentUser} from "../../auth/current-user";
import {UrlAwarePaginator} from "../pagination/url-aware-paginator.service";
import {DataTable} from "../data-table";
import {Aggregators} from "./aggregators.service";

@Component({
    selector: 'aggregators',
    templateUrl: './aggregators.component.html',
    providers: [UrlAwarePaginator],
    encapsulation: ViewEncapsulation.None,
})
export class AggregatorsComponent extends DataTable implements OnInit, OnDestroy {

    /**
     * UsersComponent Constructor.
     */
    constructor(
        public paginator: UrlAwarePaginator,
        private aggregators: Aggregators,
        private modal: ModalService,
        public currentUser: CurrentUser,
    ) {
        super();
    }

    ngOnInit() {
        this.paginator.paginate('aggregators', {order_by: 'updated_at'}).subscribe(response => {
            this.items = response.data;
        });
    }

    ngOnDestroy() {
        this.paginator.destroy();
    }

    /**
     * Ask user to confirm deletion of selected artists
     * and delete selected artists if user confirms.
     */
    public maybeDeleteSelectedAggregators() {
        this.modal.show(ConfirmModalComponent, {
            title: 'Delete Aggregators',
            body:  'Are you sure you want to delete selected artists?',
            ok:    'Delete'
        }).onDone.subscribe(() => this.deleteSelectedAggregators());
    }

    /**
     * Delete currently selected artists.
     */
    public deleteSelectedAggregators() {
        this.aggregators.delete(this.selectedItems).subscribe((a) => {
            this.paginator.refresh();
            this.selectedItems = [];
        });
    }
}
