import {Artist} from "./Artist";

export class Aggregator {
    id: number;
    username: string;
    name: string;
    user_info_id: number;
    updated_at?: string;
    bio?: string;
    address: string;
    cac_number: string;
    artists?: Artist[];

    constructor(params: Object = {}) {
        for (let name in params) {
            this[name] = params[name];
        }
    }
}
